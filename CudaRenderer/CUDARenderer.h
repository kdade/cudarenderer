// CUDA related interface

#ifndef CR_CUDA_RENDERER_H
#define CR_CUDA_RENDERER_H

// General purpose includes
#include <vector>

// OpenGL includes
#include <GLEW/glew.h>
#include <GLFW/glfw3.h>

// Local includes
#include "BasicTypes.h"
#include "CRImage.h"
#include "tiny_obj_loader.h"

using namespace std;

class CUDARenderer {
	GLuint pbo;
	GLuint tex;
	// This is where the CPU keeps everything
	// Here is the metadata - holds resolution info, and camera info
	SceneMetaData gpu_scene_data;
	// CPU data arrays pertaining to Mesh handling
	vector<Mesh> meshes;
	vector<Instance> meshInstances;
	unsigned int lastInstanceWasMesh = false;
	// These are CPU data arrays - eventually will be copied to GPU
	vector<CRImage> images;			unsigned int N_images = 0;
	vector<Light> lights;			unsigned int N_lights = 0;
	vector<Normal> normals;			unsigned int N_normals = 0;
	vector<Texcoord> texcoords;		unsigned int N_texcoords = 0;
	vector<Vertex> vertices;		unsigned int N_vertices = 0;
	vector<Triangle> triangles;		unsigned int N_triangles = 0;
	vector<Material> materials;		unsigned int N_materials = 0;
	vector<Transform> transforms;	unsigned int N_transforms = 0;
	vector<Instance> instances;		unsigned int N_instances = 0;
	vector<Sphere> spheres;			unsigned int N_spheres = 0;
	vector<Box> boxes;				unsigned int N_boxes = 0;
	// These are the alloc'd memory blocks on the GPU that need to be freed.
	bool dataAllocdOnGPU = false;

public:
	CUDARenderer::CUDARenderer(int resx, int resy); // Must be called before GL context established...
	CUDARenderer::~CUDARenderer();
	void CUDARenderer::initGLStructures(); // Must be called after GL context established...
	void CUDARenderer::refresh(int wwidth, int wheight);
	void CUDARenderer::render();
	void CUDARenderer::clearScene();
	void CUDARenderer::setBVHMode(BVHMODE mode, int param);
	void CUDARenderer::setResolution(int resx, int resy);
	void CUDARenderer::setCamera(Camera c);
	void CUDARenderer::setAntiAlias(unsigned int rate);
	int CUDARenderer::setAmbientLight(Vector3 rgb);
	int CUDARenderer::addImage(const char* filename);
	int CUDARenderer::addObject(const char* filename);
	int CUDARenderer::addLight(Light l);
	int CUDARenderer::addTransform(Transform t);
	int CUDARenderer::addVertex(Vertex v);
	int CUDARenderer::addMaterial(Material m);
	int CUDARenderer::addSphere(Sphere s);
	int CUDARenderer::addTriangle(Triangle t);
	int CUDARenderer::addBox(Box b);
	int CUDARenderer::addInstance(Instance i);
	void CUDARenderer::undoInstance();
	void CUDARenderer::dump();


private:
	void CUDARenderer::convertObjectsToPrimitives();
	void CUDARenderer::pushToGPU();
	void CUDARenderer::freeDataOnGPU();
	void CUDARenderer::buildAccelerationStructures();
};


#endif