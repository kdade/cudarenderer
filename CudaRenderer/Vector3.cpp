// And the painstaking implementation of all the operator overloading...

// This is a clever way of using the cuda syntax only when necessary - courtesy of the internet.
#ifdef FOR_CUDA
#define DEVICE_AND_HOST __device__ __host__
#else
#define DEVICE_AND_HOST 
#endif


#include <math.h>
#include <cuda_runtime.h>
#include <device_launch_parameters.h>

#include "Vector3.h"


// Constructors
DEVICE_AND_HOST Vector3::Vector3(float x, float y, float z) {
	this->x = x;
	this->y = y;
	this->z = z;
}
DEVICE_AND_HOST Vector3::Vector3(float a) {
	x = a;
	y = a;
	z = a;
}

// Equality comparison
DEVICE_AND_HOST bool Vector3::operator==(const Vector3& b) {
	return (this->x == b.x) && (this->y == b.y) && (this->z == b.z);
}

// Vector Assignment
DEVICE_AND_HOST Vector3 Vector3::operator=(const Vector3& b) {
	x = b.x; y = b.x; z = b.z;
	return *this;
}

// Vector Addition
DEVICE_AND_HOST Vector3 Vector3::operator+(const Vector3& b) {
	return Vector3(this->x + b.x, this->y + b.y, this->z + b.z);
}
DEVICE_AND_HOST Vector3 Vector3::operator+=(const Vector3& b) {
	*this = *this + b;
	return *this;
}

// Vector Subtraction
DEVICE_AND_HOST Vector3 Vector3::operator-(const Vector3& b) {
	return Vector3(this->x - b.x, this->y - b.y, this->z - b.z);
}
DEVICE_AND_HOST Vector3 Vector3::operator-=(const Vector3& b) {
	*this = *this - b;
	return *this;
}

// Vector Multiplication
DEVICE_AND_HOST Vector3 Vector3::operator*(const Vector3& b) {
	return Vector3(this->x * b.x, this->y * b.y, this->z * b.z);
}
DEVICE_AND_HOST Vector3 Vector3::operator*=(const Vector3& b) {
	*this = *this * b;
	return *this;
}

// Vector Division
DEVICE_AND_HOST Vector3 Vector3::operator/(const Vector3& b) {
	return Vector3(this->x / b.x, this->y / b.y, this->z / b.z);
}
DEVICE_AND_HOST Vector3 Vector3::operator/=(const Vector3& b) {
	*this = *this / b;
	return *this;
}

// Scalar Vector Addition
DEVICE_AND_HOST Vector3 operator+(const Vector3& v, float c) {
	return Vector3(v.x + c, v.y + c, v.z + c);
}
DEVICE_AND_HOST Vector3 operator+(float c, const Vector3& v) {
	return Vector3(v.x + c, v.y + c, v.z + c);
}

// Scalar Vector Subtraction
DEVICE_AND_HOST Vector3 operator-(const Vector3& v, float c) {
	return Vector3(v.x - c, v.y - c, v.z - c);
}
DEVICE_AND_HOST Vector3 operator-(float c, const Vector3& v) {
	return Vector3(c - v.x, c - v.y, c - v.z);
}

// Scalar Vector Multiplication
DEVICE_AND_HOST Vector3 operator*(const Vector3& v, float c) {
	return Vector3(v.x * c, v.y * c, v.z * c);
}
DEVICE_AND_HOST Vector3 operator*(float c, const Vector3& v) {
	return Vector3(v.x * c, v.y * c, v.z * c);
}

// Scalar Vector Division
DEVICE_AND_HOST Vector3 operator/(const Vector3& v, float c) {
	return Vector3(v.x / c, v.y / c, v.z / c);
}
DEVICE_AND_HOST Vector3 operator/(float c, const Vector3& v) {
	return Vector3(c / v.x, c / v.y, c / v.z);
}

// Dot Product
DEVICE_AND_HOST float dot(const Vector3& u, const Vector3& v) {
	return u.x*v.x + u.y*v.y + u.z*v.z;
}

// L2 Norm
DEVICE_AND_HOST float norm(const Vector3& v) {
	return sqrtf(dot(v, v));
}

// Cross Product
DEVICE_AND_HOST Vector3 cross(const Vector3& u, const Vector3& v) {
	return Vector3(u.y*v.z - u.z*v.y, u.z*v.x - u.x*v.z, u.x*v.y - u.y*v.x);
}

// Clamp
DEVICE_AND_HOST Vector3 clamp(const Vector3& v, float a, float b) {
	float x = (v.x < a) ? a : ((v.x > b) ? b : v.x);
	float y = (v.y < a) ? a : ((v.y > b) ? b : v.y);
	float z = (v.z < a) ? a : ((v.z > b) ? b : v.z);
	return Vector3(x, y, z);
}
DEVICE_AND_HOST Vector3 clamp(const Vector3& v) {
	return clamp(v, 0.0f, 1.0f);
}
