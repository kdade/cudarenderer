// Bare bones image class implementation

#include "CRImage.h"
#include <iostream>

using namespace std;

CRImage::CRImage() {
	width = 0;
	height = 0;
	pitch = 0;
	rgbquads = nullptr;
}

CRImage::CRImage(const char* imfile) {
	FREE_IMAGE_FORMAT fif = FIF_UNKNOWN;
	// check the file signature and deduce its format
	// (the second argument is currently not used by FreeImage)
	fif = FreeImage_GetFileType(imfile, 0);
	if (fif == FIF_UNKNOWN) {
		// no signature ?
		// try to guess the file format from the file extension
		fif = FreeImage_GetFIFFromFilename(imfile);
	}
	FIBITMAP* dib;
	// check that the plugin has reading capabilities ...
	if ((fif != FIF_UNKNOWN) && FreeImage_FIFSupportsReading(fif)) {
		// ok, let's load the file
		dib = FreeImage_Load(fif, imfile);
		cout << "Loaded image from " << imfile << endl;
	}
	else {
		dib = nullptr;
	}
	// convert a bitmap to a 32-bit raw buffer (top-left pixel first)
	// --------------------------------------------------------------
	FIBITMAP *src = FreeImage_ConvertTo32Bits(dib);
	FreeImage_Unload(dib);
	dib = nullptr;
	if (src == nullptr) {
		cout << "ERROR: Image file format not supported." << endl;
	}
	// Allocate a raw buffer
	width = FreeImage_GetWidth(src);	
	height = FreeImage_GetHeight(src);
	pitch = width * sizeof(RGBQUAD);
	rgbquads = (RGBQUAD*)malloc(width*height*sizeof(RGBQUAD));
	for (unsigned int y = 0; y < height; y++) {
		for (unsigned int x = 0; x < width; x++) {
			RGBQUAD rgbquad;
			FreeImage_GetPixelColor(src, x, y, &rgbquad);
			rgbquads[y*width + x] = rgbquad;
			//cout << "rgbquad: <" << (float)rgbquad.rgbRed / (float)255<< ", " << (float)rgbquad.rgbGreen / (float)255 << ", " << (float)rgbquad.rgbBlue / (float)255 << ", " << (float)rgbquad.rgbReserved << ">" << endl;
		}
	}
	FreeImage_Unload(src);
}

void CRImage::Cleanup() {
	free(rgbquads);
}

