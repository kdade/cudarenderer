//
// ONE VERY IMPORTANT THING
//
// ALL BASIC TYPES ARE PADDED TO BE A MULTIPLE OF 4 BYTES
//
// THIS IS NICE FOR THE TEXTURE FETCHING


#ifndef CR_BASIC_TYPES_H
#define CR_BASIC_TYPES_H


#include "Vector3.h"
#include "Transform.h"
#include "CRImage.h"

#define MAX_TEXTURES 6	// This value might still be hard-coded in other places :/

#define NAT			999999
#define TRIANGLE	0
#define SPHERE		1
#define BOX			2
#define OBJECT		3

#define M_PI 3.14159265359f

// Camera
#define CAMERA_N_ELEMS 11 // lookdir, look
struct Camera {
	Vector3 eye;
	Vector3 up;
	Vector3 look;
	float FOV;
	float aperture;
	Vector3 eyeoffset;
	__host__ Camera::Camera() {
		eye = Vector3(0);
		eyeoffset = Vector3(0.0);
		up = Vector3(0, 1, 0);
		look = Vector3(0, 0, -1);
		FOV = (float)(M_PI / 4.0f);
		aperture = 0;
	}
};

// This is a ray
struct Ray {
	Vector3 o;
	Vector3 d;
	Vector3 dinv;
	int sign[3];
	// PAD
	//float padding[3];
};

// This is a light
#define LIGHT_N_ELEMS 10
struct Light {
	Vector3 pos;
	Vector3 col;
	Vector3 dir;
	float cos_theta; // Used for directional lighting - set to -1 for point light
	// PAD
	//float padding[2];
};

// This is a vertex
#define VERTEX_N_ELEMS 3
struct Vertex {
	Vector3 pos;
	// PAD
	//float padding;
};

// This is a texture coordinate
#define TEXCOORD_N_ELEMS 2
struct Texcoord {
	float u;
	float v;
};

// This is a normal
#define NORMAL_N_ELEMS 3
struct Normal {
	Vector3 dir;
	// PAD
	//float padding;
};

// This is a material
#define MATERIAL_N_ELEMS 17
struct Material {
	Vector3 ka;	// ambient rgb
	Vector3 kd;	// diffuse rgb
	Vector3 ks;	// specular rgb
	float Ns;	// specular exponent
	Vector3 kr;	// reflected rgb
	float n;	// refraction index
	Vector3 c;	// attenuation coefficients
	// PAD
	//float padding[3];
};

// These are the available bounding volume hierarchy modes
typedef unsigned int BVHMODE;
#define BVHMODE_NONE		0
#define BVHMODE_TREE		1
#define BVHMODE_TREE_FULL	2
#define BVHMODE_UNIFORM		3
// Here is the most basic form of bounding volume
struct AABBnode {
	union {
		unsigned int instance_id;
		unsigned int firstChildIndex; // Assume that all child nodes are in a contiguous block starting here
	};
	unsigned int numChildren;
	Vector3 upperFrontRight;		// upper front right corner if looking down negative z axis	
	Vector3 lowerBackLeft;	// lower back left corner if looking down negative z axis
	__device__ float hit(Ray ray);
};

// This is a triangle
#define TRIANGLE_N_ELEMS 3
struct Triangle {
	unsigned int vert_id[3];
	unsigned int normal_id[3];
	unsigned int texcoord_id[3];
	// Flags
	union {
		struct {
			unsigned short int use_texcoords;
			unsigned short int use_normals;
		} f;
		float flags;
	};
	// These can be precomputed, used for triangle intersection.
	Vector3 e1;
	Vector3 e2;
	// PAD
	// float padding;
	// This is a function which says if it is hit or not
	__device__ float hit(Ray ray, const Transform& tform);
	__device__ AABBnode getAABB(const Transform &tform);
};

// And a sphere
#define SPHERE_N_ELEMS 4
struct Sphere {
	Vector3 pos;
	float r;
	// This is used to calculate a hit
	__device__ float hit(Ray ray, const Transform& tform);
	__device__ AABBnode getAABB(const Transform &tform);
};

// Aaaand a box...
#define BOX_N_ELEMS 8
struct Box {
	unsigned int vert_id[8];
	__device__ float hit(Ray ray, const Transform& tform);
	__device__ AABBnode getAABB(const Transform &tform);
};

// This is a generic entity in world space that the rays can intersect
#define INSTANCE_N_ELEMS 8
struct Instance {
	unsigned int type;
	unsigned int id;
	unsigned int tform_id;
	unsigned int mat_id;
	unsigned int color_tex_id;
	unsigned int normal_tex_id;
	unsigned int useColorTex;
	unsigned int useNormalTex;
	// Precomputed
	Vector3 centroid;
	// PAD
	//float padding[3];
	__device__ float hit(Ray ray);
	__device__ AABBnode getAABB();
};

// This is a triangle mesh
struct Mesh {
	vector<Vertex> vertices;
	vector<Texcoord> texcoords;
	vector<Normal> normals;
	vector<Triangle> triangles;
	Mesh::Mesh() {
		vertices = vector<Vertex>();
		texcoords = vector<Texcoord>();
		normals = vector<Normal>();
		triangles = vector<Triangle>();
	}
};

// This is the scene meta data that will be passed to the GPU
struct SceneMetaData {
	// Pixel resolution
	int resx;
	int resy;
	// Camera parameters
	Camera camera;
	unsigned int antialiasrate;
	// These are flags regarding rendering
	bool ambient_on;
	bool diffuse_on;
	bool specular_on;
	bool reflection_on;
	bool transmission_on;
	bool attenuation_on;
	// Clear color
	Vector3 clear_color;
	// This is the type of acceleration structure
	BVHMODE bvhmode;
	// This is the parameter for the BVH - usually tree radix
	unsigned int bvhparam;
	// This is a pointer to the memory containing the BVH tree
	AABBnode* bvhmem;
	bool useBVH;
	unsigned int N_bvhmaxdepth;
	unsigned int N_bvhnodes;
	unsigned int N_bvhleaves;
	// Ambient light
	Vector3 ambient_light;
	// This is the length of all arrays in global memory
	unsigned int N_images;
	unsigned int N_lights;
	unsigned int N_normals;
	unsigned int N_texcoords;
	unsigned int N_vertices;
	unsigned int N_materials;
	unsigned int N_transforms;
	unsigned int N_instances;
	unsigned int N_triangles;
	unsigned int N_spheres;
	unsigned int N_boxes;
	// These are all pointers to device global memory - to be read through texture cache
	CRImage images[MAX_TEXTURES];
	Light* lights;
	Normal* normals;
	Texcoord* texcoords;
	Vertex* vertices;
	Material* materials;
	Transform* transforms;
	Instance* instances;
	Triangle* triangles;
	Sphere* spheres;
	Box* boxes;
};

#endif