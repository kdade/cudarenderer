// Source for the windowing and OpenGL context backend
#include <iostream>

// Local includes
#include "GLFWBackend.h"

using namespace std;

// Error Callback
void ErrorCB(int error, const char* description) {
	fputs(description, stderr);
	exit(EXIT_FAILURE);
}


// Constructor -- set up GLFW window and GLEW
GLFWwindow* createWindow(int width, int height, const char* title) {
	// Begin GLFW
	glfwSetErrorCallback(ErrorCB);
	if (!glfwInit()) exit(EXIT_FAILURE);
	// Set up the window
	glfwWindowHint(GLFW_VISIBLE, false);
	GLFWwindow* window = glfwCreateWindow(width, height, title, NULL, NULL);
	// Make sure it's all good
	if (!window) {
		cout << "Error creating window." << endl;
		exit(EXIT_FAILURE);
	}
	// Start context
	glfwMakeContextCurrent(window);
	// Initialize GLEW now the openGL context is good to go
	glewExperimental = GL_TRUE;
	GLenum res = glewInit();
	if (res != GLEW_OK) {
		ErrorCB(0, (const char*)glewGetErrorString(res));
	}
	// Set up OpenGL stuff
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glEnable(GL_TEXTURE_2D);
	glViewport(0, 0, width, height);
	glDisable(GL_DEPTH_TEST);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f);

	// If successful...
	cout << "GLFW initialization success!" << endl;
	// Return pointer to the window
	return window;
}















