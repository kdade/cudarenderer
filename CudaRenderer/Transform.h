// This is used for transforming Vector3 s

#ifndef CR_TRANSFORM_H
#define CR_TRANSFORM_H	

// This is a clever way of using the cuda syntax only when necessary - courtesy of the internet.
#ifdef CUDA_VERSION
#define DEVICE_AND_HOST __device__ __host__
#else
#define DEVICE_AND_HOST
#endif


#include "Vector3.h"

#define TRANSFORM_N_ELEMS 16
#define TRANSFORM_F4 4

#define POINT 'p'
#define VECTOR 'v'

//////////// Standard graphics Mat4 transform ////////////////
struct Transform {
	float A[4][4];
	float AinvT[4][4];
	// Constructors
	DEVICE_AND_HOST Transform(float B[4][4]);
	DEVICE_AND_HOST Transform();
	// Overload operators
	DEVICE_AND_HOST Transform operator*(const Transform&);
	// Allow for right multiplying vector3
	DEVICE_AND_HOST friend Vector3 transformPoint(const Transform&, const Vector3&);
	DEVICE_AND_HOST friend Vector3 transformVector(const Transform&, const Vector3&);
};


/////////////////////////////////////////////////////////////////////////
// Implementation
/////////////////////////////////////////////////////////////////////////


#ifdef FOR_CUDA

DEVICE_AND_HOST Transform::Transform(float B[4][4]) {
	A[0][0] = B[0][0];	A[0][1] = B[0][1];	A[0][2] = B[0][2];	A[0][3] = B[0][3];
	A[1][0] = B[1][0];	A[1][1] = B[1][1];	A[1][2] = B[1][2];	A[1][3] = B[1][3];
	A[2][0] = B[2][0];	A[2][1] = B[2][1];	A[2][2] = B[2][2];	A[2][3] = B[2][3];
	A[3][0] = B[3][0];	A[3][1] = B[3][1];	A[3][2] = B[3][2];	A[3][3] = B[3][3];
	
	// Calculate transpose inverse
	float det;
	float inv[16], m[16];
	for (int i = 0; i < 4; i++) {
		for (int j = 0; j < 4; j++) {
			m[i*4 + j] = B[i][j];
		}
	}
	inv[0] = m[5]  * m[10] * m[15] - 
		m[5]  * m[11] * m[14] - 
		m[9]  * m[6]  * m[15] + 
		m[9]  * m[7]  * m[14] +
		m[13] * m[6]  * m[11] - 
		m[13] * m[7]  * m[10];

	inv[4] = -m[4]  * m[10] * m[15] + 
		m[4]  * m[11] * m[14] + 
		m[8]  * m[6]  * m[15] - 
		m[8]  * m[7]  * m[14] - 
		m[12] * m[6]  * m[11] + 
		m[12] * m[7]  * m[10];

	inv[8] = m[4]  * m[9] * m[15] - 
		m[4]  * m[11] * m[13] - 
		m[8]  * m[5] * m[15] + 
		m[8]  * m[7] * m[13] + 
		m[12] * m[5] * m[11] - 
		m[12] * m[7] * m[9];

	inv[12] = -m[4]  * m[9] * m[14] + 
		m[4]  * m[10] * m[13] +
		m[8]  * m[5] * m[14] - 
		m[8]  * m[6] * m[13] - 
		m[12] * m[5] * m[10] + 
		m[12] * m[6] * m[9];

	inv[1] = -m[1]  * m[10] * m[15] + 
		m[1]  * m[11] * m[14] + 
		m[9]  * m[2] * m[15] - 
		m[9]  * m[3] * m[14] - 
		m[13] * m[2] * m[11] + 
		m[13] * m[3] * m[10];

	inv[5] = m[0]  * m[10] * m[15] - 
		m[0]  * m[11] * m[14] - 
		m[8]  * m[2] * m[15] + 
		m[8]  * m[3] * m[14] + 
		m[12] * m[2] * m[11] - 
		m[12] * m[3] * m[10];

	inv[9] = -m[0]  * m[9] * m[15] + 
		m[0]  * m[11] * m[13] + 
		m[8]  * m[1] * m[15] - 
		m[8]  * m[3] * m[13] - 
		m[12] * m[1] * m[11] + 
		m[12] * m[3] * m[9];

	inv[13] = m[0]  * m[9] * m[14] - 
		m[0]  * m[10] * m[13] - 
		m[8]  * m[1] * m[14] + 
		m[8]  * m[2] * m[13] + 
		m[12] * m[1] * m[10] - 
		m[12] * m[2] * m[9];

	inv[2] = m[1] * m[6] * m[15] -
		m[1] * m[7] * m[14] -
		m[5] * m[2] * m[15] +
		m[5] * m[3] * m[14] +
		m[13] * m[2] * m[7] -
		m[13] * m[3] * m[6];

	inv[6] = -m[0] * m[6] * m[15] +
		m[0] * m[7] * m[14] +
		m[4] * m[2] * m[15] -
		m[4] * m[3] * m[14] -
		m[12] * m[2] * m[7] +
		m[12] * m[3] * m[6];

	inv[10] = m[0] * m[5] * m[15] -
		m[0] * m[7] * m[13] -
		m[4] * m[1] * m[15] +
		m[4] * m[3] * m[13] +
		m[12] * m[1] * m[7] -
		m[12] * m[3] * m[5];

	inv[14] = -m[0] * m[5] * m[14] +
		m[0] * m[6] * m[13] +
		m[4] * m[1] * m[14] -
		m[4] * m[2] * m[13] -
		m[12] * m[1] * m[6] +
		m[12] * m[2] * m[5];

	inv[3] = -m[1] * m[6] * m[11] +
		m[1] * m[7] * m[10] +
		m[5] * m[2] * m[11] -
		m[5] * m[3] * m[10] -
		m[9] * m[2] * m[7] +
		m[9] * m[3] * m[6];

	inv[7] = m[0] * m[6] * m[11] -
		m[0] * m[7] * m[10] -
		m[4] * m[2] * m[11] +
		m[4] * m[3] * m[10] +
		m[8] * m[2] * m[7] -
		m[8] * m[3] * m[6];

	inv[11] = -m[0] * m[5] * m[11] +
		m[0] * m[7] * m[9] +
		m[4] * m[1] * m[11] -
		m[4] * m[3] * m[9] -
		m[8] * m[1] * m[7] +
		m[8] * m[3] * m[5];

	inv[15] = m[0] * m[5] * m[10] -
		m[0] * m[6] * m[9] -
		m[4] * m[1] * m[10] +
		m[4] * m[2] * m[9] +
		m[8] * m[1] * m[6] -
		m[8] * m[2] * m[5];

	det = m[0] * inv[0] + m[1] * inv[4] + m[2] * inv[8] + m[3] * inv[12];

	if (det == 0)
		det = FLT_MAX;

	det = 1.0f / det;

	for (int i = 0; i < 4; i++) {
		for (int j = 0; j < 4; j++) {
			AinvT[j][i] = inv[i*4 + j] * det;
		}
	}
	// Thanks "shoosh" from stack-overflow!!!
}
// Makes the identity
DEVICE_AND_HOST Transform::Transform() {
	A[0][0] = 1;	A[0][1] = 0;	A[0][2] = 0;	A[0][3] = 0;
	A[1][0] = 0;	A[1][1] = 1;	A[1][2] = 0;	A[1][3] = 0;
	A[2][0] = 0;	A[2][1] = 0;	A[2][2] = 1;	A[2][3] = 0;
	A[3][0] = 0;	A[3][1] = 0;	A[3][2] = 0;	A[3][3] = 1;

	AinvT[0][0] = 1;	AinvT[0][1] = 0;	AinvT[0][2] = 0;	AinvT[0][3] = 0;
	AinvT[1][0] = 0;	AinvT[1][1] = 1;	AinvT[1][2] = 0;	AinvT[1][3] = 0;
	AinvT[2][0] = 0;	AinvT[2][1] = 0;	AinvT[2][2] = 1;	AinvT[2][3] = 0;
	AinvT[3][0] = 0;	AinvT[3][1] = 0;	AinvT[3][2] = 0;	AinvT[3][3] = 1;

}
// Multiply two matrices
DEVICE_AND_HOST Transform Transform::operator*(const Transform& B) {
	float C[4][4];
	C[0][0] = A[0][0] * B.A[0][0] + A[0][1] * B.A[1][0] + A[0][2] * B.A[2][0] + A[0][3] * B.A[3][0];
	C[1][0] = A[1][0] * B.A[0][0] + A[1][1] * B.A[1][0] + A[1][2] * B.A[2][0] + A[1][3] * B.A[3][0];
	C[2][0] = A[2][0] * B.A[0][0] + A[2][1] * B.A[1][0] + A[2][2] * B.A[2][0] + A[2][3] * B.A[3][0];
	C[3][0] = A[3][0] * B.A[0][0] + A[3][1] * B.A[1][0] + A[3][2] * B.A[2][0] + A[3][3] * B.A[3][0];

	C[0][1] = A[0][0] * B.A[0][1] + A[0][1] * B.A[1][1] + A[0][2] * B.A[2][1] + A[0][3] * B.A[3][1];
	C[1][1] = A[1][0] * B.A[0][1] + A[1][1] * B.A[1][1] + A[1][2] * B.A[2][1] + A[1][3] * B.A[3][1];
	C[2][1] = A[2][0] * B.A[0][1] + A[2][1] * B.A[1][1] + A[2][2] * B.A[2][1] + A[2][3] * B.A[3][1];
	C[3][1] = A[3][0] * B.A[0][1] + A[3][1] * B.A[1][1] + A[3][2] * B.A[2][1] + A[3][3] * B.A[3][1];

	C[0][2] = A[0][0] * B.A[0][2] + A[0][1] * B.A[1][2] + A[0][2] * B.A[2][2] + A[0][3] * B.A[3][2];
	C[1][2] = A[1][0] * B.A[0][2] + A[1][1] * B.A[1][2] + A[1][2] * B.A[2][2] + A[1][3] * B.A[3][2];
	C[2][2] = A[2][0] * B.A[0][2] + A[2][1] * B.A[1][2] + A[2][2] * B.A[2][2] + A[2][3] * B.A[3][2];
	C[3][2] = A[3][0] * B.A[0][2] + A[3][1] * B.A[1][2] + A[3][2] * B.A[2][2] + A[3][3] * B.A[3][2];

	C[0][3] = A[0][0] * B.A[0][3] + A[0][1] * B.A[1][3] + A[0][2] * B.A[2][3] + A[0][3] * B.A[3][3];
	C[1][3] = A[1][0] * B.A[0][3] + A[1][1] * B.A[1][3] + A[1][2] * B.A[2][3] + A[1][3] * B.A[3][3];
	C[2][3] = A[2][0] * B.A[0][3] + A[2][1] * B.A[1][3] + A[2][2] * B.A[2][3] + A[2][3] * B.A[3][3];
	C[3][3] = A[3][0] * B.A[0][3] + A[3][1] * B.A[1][3] + A[3][2] * B.A[2][3] + A[3][3] * B.A[3][3];
	return Transform(C);
}
// Multiply a point
DEVICE_AND_HOST Vector3 transformPoint(const Transform& T, const Vector3& v) {
	float x = T.A[0][0] * v.x + T.A[0][1] * v.y + T.A[0][2] * v.z + T.A[0][3] * 1.0f;
	float y = T.A[1][0] * v.x + T.A[1][1] * v.y + T.A[1][2] * v.z + T.A[1][3] * 1.0f;
	float z = T.A[2][0] * v.x + T.A[2][1] * v.y + T.A[2][2] * v.z + T.A[2][3] * 1.0f;
	return Vector3(x, y, z);
}
// Multiply a vector
DEVICE_AND_HOST Vector3 transformVector(const Transform& T, const Vector3& v) {
	float x = T.A[0][0] * v.x + T.A[0][1] * v.y + T.A[0][2] * v.z;
	float y = T.A[1][0] * v.x + T.A[1][1] * v.y + T.A[1][2] * v.z;
	float z = T.A[2][0] * v.x + T.A[2][1] * v.y + T.A[2][2] * v.z;
	return Vector3(x, y, z);
}

// Multiply a normal
DEVICE_AND_HOST Vector3 transformNormal(const Transform& T, const Vector3& v) {
	float x = T.AinvT[0][0] * v.x + T.AinvT[0][1] * v.y + T.AinvT[0][2] * v.z;
	float y = T.AinvT[1][0] * v.x + T.AinvT[1][1] * v.y + T.AinvT[1][2] * v.z;
	float z = T.AinvT[2][0] * v.x + T.AinvT[2][1] * v.y + T.AinvT[2][2] * v.z;
	return normalize(Vector3(x, y, z));
}

#endif


#endif