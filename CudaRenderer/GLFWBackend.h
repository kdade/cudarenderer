// Takes care of OpenGL context and windowing

#ifndef CR_GLFW_BACKEND_H
#define CR_GLFW_BACKEND_H

// Using both of these
#include <GLEW/glew.h>
#include <GLFW/glfw3.h>

GLFWwindow* createWindow(int width, int height, const char* title);

#endif