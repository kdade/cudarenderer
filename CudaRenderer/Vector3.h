// Contains all the structs (and their device functions) to be used

#ifndef CR_VECTOR3_H
#define CR_VECTOR3_H

// This is a clever way of using the cuda syntax only when necessary - courtesy of the internet.
#ifdef FOR_CUDA
#define DEVICE_AND_HOST __device__ __host__
#else
#define DEVICE_AND_HOST
#endif

#include <math.h>

/////// BASIC PRIMITIVES - DON'T LEAVE HOME WITHOUT THEM ////////////////

// This is the most basic thing we need
struct Vector3 {
	float x, y, z;
	// Constructors
	DEVICE_AND_HOST Vector3(float, float, float);
	DEVICE_AND_HOST Vector3(float);
	DEVICE_AND_HOST Vector3();
	// And overload operators -------
	// These are all between vectors
	DEVICE_AND_HOST bool operator==(const Vector3&);
		
	DEVICE_AND_HOST Vector3 operator=(const Vector3&);
	DEVICE_AND_HOST Vector3 operator+(const Vector3&);
	DEVICE_AND_HOST Vector3 operator-(const Vector3&);
	DEVICE_AND_HOST Vector3 operator*(const Vector3&);
	DEVICE_AND_HOST Vector3 operator/(const Vector3&);

	DEVICE_AND_HOST Vector3 operator+=(const Vector3&);
	DEVICE_AND_HOST Vector3 operator-=(const Vector3&);
	DEVICE_AND_HOST Vector3 operator*=(const Vector3&);
	DEVICE_AND_HOST Vector3 operator/=(const Vector3&);

	DEVICE_AND_HOST Vector3 operator+() const { return *this; };
	DEVICE_AND_HOST Vector3 operator-() const { return Vector3(-x, -y, -z); };


	// These are between a scalar and a vector
	DEVICE_AND_HOST Vector3 operator+(float);
	DEVICE_AND_HOST Vector3 operator-(float);
	DEVICE_AND_HOST Vector3 operator*(float);
	DEVICE_AND_HOST Vector3 operator/(float);

	DEVICE_AND_HOST friend Vector3 operator+(float, const Vector3&);
	DEVICE_AND_HOST friend Vector3 operator-(float, const Vector3&);
	DEVICE_AND_HOST friend Vector3 operator*(float, const Vector3&);
	DEVICE_AND_HOST friend Vector3 operator/(float, const Vector3&);

	// Other useful functions
	DEVICE_AND_HOST static friend float dot(const Vector3&, const Vector3&);
	DEVICE_AND_HOST static friend float norm(const Vector3&);
	DEVICE_AND_HOST static friend Vector3 cross(const Vector3&, const Vector3&);
	DEVICE_AND_HOST static friend Vector3 clamp(const Vector3&, float, float);
	DEVICE_AND_HOST static friend Vector3 clamp(const Vector3&);
	DEVICE_AND_HOST static friend Vector3 normalize(const Vector3&);
	// Need this for host
	static friend Vector3 host_cross(const Vector3&, const Vector3&);
	static friend float host_dot(const Vector3&, const Vector3&);
	static friend float host_norm(const Vector3&);
	static friend Vector3 host_normalize(const Vector3&);
};

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
// ----------------------------- Function Implementation -------------------------------------------
////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef FOR_CUDA
// Constructors
DEVICE_AND_HOST Vector3::Vector3(float a, float b, float c) {
	this->x = a;
	this->y = b;
	this->z = c;
}
DEVICE_AND_HOST Vector3::Vector3(float a) {
	x = a;
	y = a;
	z = a;
}

DEVICE_AND_HOST Vector3::Vector3() {
	x = 0.0;
	y = 0.0;
	z = 0.0;
}

// Equality comparison
DEVICE_AND_HOST bool Vector3::operator==(const Vector3& b) {
	return (this->x == b.x) && (this->y == b.y) && (this->z == b.z);
}

// Vector Assignment
DEVICE_AND_HOST Vector3 Vector3::operator=(const Vector3& b) {
	x = b.x; y = b.y; z = b.z;
	return *this;
}

// Vector Addition
DEVICE_AND_HOST Vector3 Vector3::operator+(const Vector3& b) {
	return Vector3(this->x + b.x, this->y + b.y, this->z + b.z);
}
DEVICE_AND_HOST Vector3 Vector3::operator+=(const Vector3& b) {
	*this = *this + b;
	return *this;
}

// Vector Subtraction
DEVICE_AND_HOST Vector3 Vector3::operator-(const Vector3& b) {
	return Vector3(this->x - b.x, this->y - b.y, this->z - b.z);
}
DEVICE_AND_HOST Vector3 Vector3::operator-=(const Vector3& b) {
	*this = *this - b;
	return *this;
}

// Vector Multiplication
DEVICE_AND_HOST Vector3 Vector3::operator*(const Vector3& b) {
	return Vector3(this->x * b.x, this->y * b.y, this->z * b.z);
}
DEVICE_AND_HOST Vector3 Vector3::operator*=(const Vector3& b) {
	*this = *this * b;
	return *this;
}

// Vector Division
DEVICE_AND_HOST Vector3 Vector3::operator/(const Vector3& b) {
	return Vector3(this->x / b.x, this->y / b.y, this->z / b.z);
}
DEVICE_AND_HOST Vector3 Vector3::operator/=(const Vector3& b) {
	*this = *this / b;
	return *this;
}

// Scalar Vector Addition
DEVICE_AND_HOST Vector3 Vector3::operator+(float c) {
	return Vector3(this->x + c, this->y + c, this->z + c);
}
DEVICE_AND_HOST Vector3 operator+(float c, const Vector3& v) {
	return Vector3(v.x + c, v.y + c, v.z + c);
}

// Scalar Vector Subtraction
DEVICE_AND_HOST Vector3 Vector3::operator-(float c) {
	return Vector3(this->x - c, this->y - c, this->z - c);
}
DEVICE_AND_HOST Vector3 operator-(float c, const Vector3& v) {
	return Vector3(c - v.x, c - v.y, c - v.z);
}

// Scalar Vector Multiplication
DEVICE_AND_HOST Vector3 Vector3::operator*(float c) {
	return Vector3(this->x * c, this->y * c, this->z * c);
}
DEVICE_AND_HOST Vector3 operator*(float c, const Vector3& v) {
	return Vector3(v.x * c, v.y * c, v.z * c);
}

// Scalar Vector Division
DEVICE_AND_HOST Vector3 Vector3::operator/(float c) {
	return Vector3(this->x / c, this->y / c, this->z / c);
}
DEVICE_AND_HOST Vector3 operator/(float c, const Vector3& v) {
	return Vector3(c / v.x, c / v.y, c / v.z);
}

// Dot Product
DEVICE_AND_HOST float dot(const Vector3& u, const Vector3& v) {
	return u.x*v.x + u.y*v.y + u.z*v.z;
}

// L2 Norm
DEVICE_AND_HOST float norm(const Vector3& v) {
	return sqrtf(dot(v, v));
}

// Cross Product
DEVICE_AND_HOST Vector3 cross(const Vector3& u, const Vector3& v) {
	return Vector3(u.y*v.z - u.z*v.y, u.z*v.x - u.x*v.z, u.x*v.y - u.y*v.x);
}

// Clamp
DEVICE_AND_HOST Vector3 clamp(const Vector3& v, float a, float b) {
	float x = (v.x < a) ? a : ((v.x > b) ? b : v.x);
	float y = (v.y < a) ? a : ((v.y > b) ? b : v.y);
	float z = (v.z < a) ? a : ((v.z > b) ? b : v.z);
	return Vector3(x, y, z);
}
DEVICE_AND_HOST Vector3 clamp(const Vector3& v) {
	return clamp(v, 0.0f, 1.0f);
}

// Normalize a vector
DEVICE_AND_HOST Vector3 normalize(const Vector3& v) {
	float n = norm(v);
	return Vector3(v.x / n, v.y / n, v.z / n);
}

#endif

// Cross Product
Vector3 host_cross(const Vector3& u, const Vector3& v) {
	return Vector3(u.y*v.z - u.z*v.y, u.z*v.x - u.x*v.z, u.x*v.y - u.y*v.x);
}

// Dot Product
float host_dot(const Vector3& u, const Vector3& v) {
	return u.x*v.x + u.y*v.y + u.z*v.z;
}

// L2 Norm
float host_norm(const Vector3& v) {
	return sqrtf(host_dot(v, v));
}

// Normalize a vector
Vector3 host_normalize(const Vector3& v) {
	float n = host_norm(v);
	return Vector3(v.x / n, v.y / n, v.z / n);
}

#endif
