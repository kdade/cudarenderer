// Where the magic happens

// Necessary for Vector3.h
#define FOR_CUDA

// This needs to have windows included...
#include <Windows.h>
#include <float.h>
#include <vector>
#include <algorithm>
#include <assert.h>
#include <iostream>
#include <fstream>

// CUDA includes
#include <cuda.h>
#include <cuda_runtime.h>
#include <cuda_gl_interop.h>
#include <device_functions.h>
#include <device_launch_parameters.h>

// Local includes
#include "HANDLE_ERROR.h"
#include "BasicTypes.h"

// These are the data textures
texture<float, 1, cudaReadModeElementType> lights_tex;	
texture<float, 1, cudaReadModeElementType> normals_tex;
texture<float, 1, cudaReadModeElementType> texcoords_tex;
texture<float, 1, cudaReadModeElementType> vertices_tex;	// <-- not actually an error
texture<float, 1, cudaReadModeElementType> materials_tex;	// <-- not actually an error
texture<float, 1, cudaReadModeElementType> transforms_tex;	// <-- not actually an error
texture<float, 1, cudaReadModeElementType> instances_tex;		// <-- not actually an error
texture<float, 1, cudaReadModeElementType> triangles_tex;		// <-- not actually an error
texture<float, 1, cudaReadModeElementType> spheres_tex;			// <-- not actually an error
texture<float, 1, cudaReadModeElementType> boxes_tex;			// <-- not actually an error
texture<float, 1, cudaReadModeElementType> bvh_tex;
// Actual textures...
texture<float, cudaTextureType2D, cudaReadModeElementType> image_texture_0;
texture<float, cudaTextureType2D, cudaReadModeElementType> image_texture_1;
texture<float, cudaTextureType2D, cudaReadModeElementType> image_texture_2;
texture<float, cudaTextureType2D, cudaReadModeElementType> image_texture_3;
texture<float, cudaTextureType2D, cudaReadModeElementType> image_texture_4;
texture<float, cudaTextureType2D, cudaReadModeElementType> image_texture_5;
// Not pretty, but we need these for access
size_t image_texture_0_offset;
size_t image_texture_1_offset;
size_t image_texture_2_offset;
size_t image_texture_3_offset;
size_t image_texture_4_offset;
size_t image_texture_5_offset;

// Intellisense is being a pain in the ass...
#define LIGHTS_TEX		lights_tex
#define NORMALS_TEX		normals_tex
#define TEXCOORDS_TEX	texcoords_tex
#define VERTICES_TEX	vertices_tex
#define MATERIALS_TEX	materials_tex
#define TRANSFORMS_TEX	transforms_tex
#define INSTANCES_TEX	instances_tex
#define TRIANGLES_TEX	triangles_tex
#define SPHERES_TEX		spheres_tex
#define BOXES_TEX		boxes_tex
#define BVH_TEX			bvh_tex
// A function to set them up and helper "getter" functions
unsigned int SetupSceneBoundingVolumeOnly(SceneMetaData scenedata);
unsigned int SetupTreeBVH(SceneMetaData scenedata);
void MortonOrderAABBs(SceneMetaData scenedata);
void SetupTextures(SceneMetaData scenedata);
void UnbindTextures();
__device__  Light getLight(unsigned int index);
__device__  Normal getNormal(unsigned int index);
__device__  Vertex getVertex(unsigned int index);
__device__  Material getMaterial(unsigned int index);
__device__  Transform getTransform(unsigned int index);
__device__  Instance getInstance(unsigned int index);
__device__  Triangle getTriangle(unsigned int index);
__device__  Sphere getSphere(unsigned int index);
__device__  Box getBox(unsigned int index);
__device__  AABBnode getAABBnode(unsigned int index);
// Texture fetching
 __device__ Vector3 imageTexFetch2D(unsigned int id, float x, float y);
// Need this for extracting uints
__device__ __inline__ unsigned int __recastf2uint(float f) { return *((unsigned int*)&f);  };

// This is a CPU struct used for sorting the AABBs
struct cpuAABB {
	UINT64 mcode;
	unsigned int sort_id;
	__host__ bool operator< (const cpuAABB& node) {
		return mcode < node.mcode;
	}
};

// This is a debugging assist
void CheckAllGPUMemory(SceneMetaData scenedata);

// ----------------------------------------------------------------------------
// Implementation of "getAABB()" functions
// ----------------------------------------------------------------------------
// Get AABB of instance
__device__ AABBnode Instance::getAABB() {
	AABBnode node;
	Triangle triangle;
	Sphere sphere;
	Box box;
	switch (type) {
	case TRIANGLE:
		triangle = getTriangle(id);
		node = triangle.getAABB(getTransform(tform_id));
		break;
	case SPHERE:
		sphere = getSphere(id);
		node = sphere.getAABB(getTransform(tform_id));
		break;
	case BOX:
		box = getBox(id);
		node = box.getAABB(getTransform(tform_id));
		break;
	}
	return node;
}

// Get AABB of triangle
__device__ AABBnode Triangle::getAABB(const Transform &tform) {
	// Get tranformed vertices
	Vector3 upperFrontRight = Vector3(-FLT_MAX, -FLT_MAX, -FLT_MAX);
	Vector3 lowerBackLeft = Vector3(FLT_MAX, FLT_MAX, FLT_MAX);
	Vector3 point;
	for (int i = 0; i < 3; i++) {
		point = transformPoint(tform, getVertex(vert_id[i]).pos);
		// x comparison
		if (point.x > upperFrontRight.x) upperFrontRight.x = point.x;
		if (point.x < lowerBackLeft.x) lowerBackLeft.x = point.x;
		// y comparison
		if (point.y > upperFrontRight.y) upperFrontRight.y = point.y;
		if (point.y < lowerBackLeft.y) lowerBackLeft.y = point.y;
		// z comparison
		if (point.z > upperFrontRight.z) upperFrontRight.z = point.z;
		if (point.z < lowerBackLeft.z) lowerBackLeft.z = point.z;
	}
	// Build the node
	AABBnode aabb;
	aabb.upperFrontRight = upperFrontRight;
	aabb.lowerBackLeft = lowerBackLeft;
	return aabb;
}

// Get AABB of sphere
__device__ AABBnode Sphere::getAABB(const Transform &tform) {
	// Get transformed center of sphere
	Vector3 center = transformPoint(tform, pos);
	// Build the node
	AABBnode aabb;
	aabb.upperFrontRight = center + Vector3(r, r, r);
	aabb.lowerBackLeft = center - Vector3(r, r, r);
	return aabb;
}

// Get AABB of box
__device__ AABBnode Box::getAABB(const Transform &tform) {
	// Get tranformed vertices
	Vector3 upperFrontRight = Vector3(-FLT_MAX, -FLT_MAX, -FLT_MAX);
	Vector3 lowerBackLeft = Vector3(FLT_MAX, FLT_MAX, FLT_MAX);
	Vector3 point;
	for (int i = 0; i < 8; i++) {
		point = transformPoint(tform, getVertex(vert_id[i]).pos);
		// x comparison
		if (point.x > upperFrontRight.x) upperFrontRight.x = point.x;
		if (point.x < lowerBackLeft.x) lowerBackLeft.x = point.x;
		// y comparison
		if (point.y > upperFrontRight.y) upperFrontRight.y = point.y;
		if (point.y < lowerBackLeft.y) lowerBackLeft.y = point.y;
		// z comparison
		if (point.z > upperFrontRight.z) upperFrontRight.z = point.z;
		if (point.z < lowerBackLeft.z) lowerBackLeft.z = point.z;
	}
	// Build the node
	AABBnode aabb;
	aabb.upperFrontRight = upperFrontRight;
	aabb.lowerBackLeft = lowerBackLeft;
	return aabb;
}

// ----------------------------------------------------------------------------
// Implementation of "hit()" functions
// ----------------------------------------------------------------------------
// Leeway for hitting objects
#define HIT_EPSILON 1e-6f
// Hit for instance
__device__ float Instance::hit(Ray ray) {
	switch (type) {
	case TRIANGLE:
		return getTriangle(id).hit(ray, getTransform(tform_id));
	case SPHERE:
		return getSphere(id).hit(ray, getTransform(tform_id));
	case BOX:
		return getBox(id).hit(ray, getTransform(tform_id));
	default:
		return -1.0;
	}
}

// Hit for triangle
__device__ float Triangle::hit(Ray ray, const Transform& tform) {
	Vector3 te1 = transformVector(tform, e1);
	Vector3 te2 = transformVector(tform, e2);
	// Triangle intersection routing
	Vector3 pvec = cross(ray.d, te2);
	float det = dot(te1, pvec);
	if (fabs(det) < 1e-4) return -1.0;
	float invDet = 1 / det;
	Vector3 tvec = ray.o - transformPoint(tform, getVertex(vert_id[0]).pos);
	float u = dot(tvec, pvec)*invDet;
	if (u < 0 - HIT_EPSILON || u > 1 + HIT_EPSILON) return -1.0;
	Vector3 qvec = cross(tvec, te1);
	float v = dot(ray.d, qvec) * invDet;
	if (v < 0 - HIT_EPSILON || v > 1 + HIT_EPSILON || u + v > 1 + 2*HIT_EPSILON) return -1.0;
	float t = dot(te2, qvec) * invDet;
	return t;
}
// Hit for sphere
__device__ float Sphere::hit(Ray ray, const Transform& tform) {
	// Sphere intersection routine
	Vector3 L = transformPoint(tform, pos) - ray.o;
	float tca = dot(L, ray.d);
	if (tca < 0.0) return -1.0;
	float d = sqrtf(dot(L, L) - tca*tca);
	if (d > r) return -1.0;
	float thc = sqrtf(r*r - d*d);
	if (tca - thc < HIT_EPSILON) return tca + thc;
	return tca - thc;
}
// Hit for box
__device__ float Box::hit(Ray ray, const Transform& tform) {
	return -1.0;
}

// Hit for AABB -- courtesy of "An Efficient and Robust Ray-Box Intersection Algorithm" - Amy Williams et. al
__device__ float AABBnode::hit(Ray ray) {
	float tmin, tmax, tymin, tymax, tzmin, tzmax; 
	Vector3 minv = lowerBackLeft - Vector3(HIT_EPSILON);
	Vector3 maxv = upperFrontRight - Vector3(HIT_EPSILON);

	tmin = ((ray.sign[0] ? maxv : minv).x - ray.o.x) * ray.dinv.x;
	tmax = ((ray.sign[0] ? minv : maxv).x - ray.o.x) * ray.dinv.x;
	tymin = ((ray.sign[1] ? maxv : minv).y - ray.o.y) * ray.dinv.y;
	tymax = ((ray.sign[1] ? minv : maxv).y - ray.o.y) * ray.dinv.y;
	if ((tmin > tymax) || (tymin > tmax))
		return false;
	if (tymin > tmin)
		tmin = tymin;
	if (tymax < tmax)
		tmax = tymax;
	tzmin = ((ray.sign[2] ? maxv : minv).z - ray.o.z) * ray.dinv.z;
	tzmax = ((ray.sign[2] ? minv : maxv).z - ray.o.z) * ray.dinv.z;
	if ((tmin > tzmax) || tzmin > tmax)
		return false;
	if (tzmin > tmin)
		tmin = tzmin;
	if (tzmax < tmax)
		tmax = tzmax;
	return tmax;
}

// ----------------------------------------------------------------------------
// Raytracing functions
// ----------------------------------------------------------------------------
// End condition - (for template recursion)
#define MAX_RAY_DEPTH 7
#define RECURSION_STACK_MAX_DEPTH 32
#define NORMAL_EPSILON 1e-3
// Gets the initial ray from the camera through the pixel
__device__ Ray getRayThroughPixel(float sx, float sy, Camera camera) {
	// Distance from viewplane to eye
	float distance = norm(camera.look - camera.eye);
	float s = 0.5 * distance * tanf(camera.FOV);
	// Get vectors spanning the screen.
	Vector3 dx = s * normalize(cross(camera.look - camera.eye, camera.up));
	Vector3 dy = s * normalize(cross(dx, camera.look - camera.eye));
	// Get this is the point to aim at
	Vector3 aim = camera.look + sx*dx + sy*dy;
	// Done
	Ray ray;
	ray.o = camera.eye + camera.eyeoffset;
	ray.d = normalize(aim - ray.o);
	ray.dinv = 1 / ray.d;
	ray.sign[0] = ray.dinv.x < 0;
	ray.sign[1] = ray.dinv.y < 0;
	ray.sign[2] = ray.dinv.z < 0;
	return ray;
}

// Shadow ray compute
__device__ __inline__ bool PointsHaveVisibility(Vector3 p1, Vector3 p2, const unsigned int& root, const SceneMetaData& scenedata) {
	float t_max = norm(p2 - p1);
	Ray ray;
	ray.o = p1;
	ray.d = normalize(p2 - p1);
	ray.dinv = 1 / ray.d;
	ray.sign[0] = ray.dinv.x < 0;
	ray.sign[1] = ray.dinv.y < 0;
	ray.sign[2] = ray.dinv.z < 0;
	unsigned int nodestack[RECURSION_STACK_MAX_DEPTH];
	unsigned int idx = 0;
	// Push the root onto the top of the stack.
	nodestack[idx++] = root;
	// Enter loop through BVH - until the index is again 0, meaning all paths have been explored
	AABBnode node;
	Instance instance;
	while (idx > 0) {
		// Get the node from the previous place in the stack
		node = getAABBnode(nodestack[idx - 1]);
		// Done with this box
		idx--;
		// Only proceed if we hit the node
		if (node.hit(ray)) {
			// If this node is a leaf, we need to run intersection on the corresponding instance
			if (node.numChildren == 0) {
				instance = getInstance(node.instance_id);
				float t_hit = instance.hit(ray);
				// Check to see if we hit the enclosed instance
				if (t_hit > HIT_EPSILON && t_hit < t_max) {
					return false;
				}
			}
			// If this node is not a leaf, we need to push its children onto the stack
			else {
				for (int n = 0; n < node.numChildren; n++) {
					nodestack[idx++] = node.firstChildIndex + n;
					if (idx >= RECURSION_STACK_MAX_DEPTH) return false; // If for some reason the stack runs out, return RED
				}
			}
		}
	}
	return true;
}

// Direct lighting
__device__ Vector3 computeDirectLighting(Ray ray, float t, const Instance& instance, const unsigned int& root, const SceneMetaData& scenedata) {
	Vector3 color(0.0);
	Material material = getMaterial(instance.mat_id);
	// Ambient light
	if (scenedata.ambient_on) {
		Vector3 amb = scenedata.ambient_light;
		color += amb * material.ka;
	}
	// Calculate View direction and Normal
	Vector3 V = -ray.d;
	Vector3 N(0.0);
	Vector3 point = ray.o + t * ray.d;
	if (instance.type == TRIANGLE) {
		Triangle triangle = getTriangle(instance.id);
		N = normalize(cross(triangle.e1, triangle.e2));
	}
	else if (instance.type == SPHERE) {
		Sphere sphere = getSphere(instance.id);
		Transform tform = getTransform(instance.tform_id);
		N = normalize(point - transformPoint(tform, sphere.pos));
	}
	// Check for occlusion with all light sources
	for (int n = 0; n < scenedata.N_lights; n++) {
		Light light = getLight(n);
		Vector3 L = normalize(light.pos - point);
		// Check for inside cone (if directional) 
		if ((light.cos_theta > -1) && dot(-L, light.dir) < light.cos_theta) return color;
		// Check occlusion
		if (PointsHaveVisibility(point + HIT_EPSILON * N, light.pos, root, scenedata)) {
			// Diffuse light
			if (scenedata.diffuse_on) {
				color += dot(N, L) * material.kd * light.col;
			}
			// Specular light
			if (scenedata.specular_on) {
				color += pow(dot((2 * dot(L, N) * N - L), V), material.Ns) * material.ks * light.col;
			}
		}
	}
	return clamp(color);
}

// Recursive raytracing
template <unsigned int depth>
__device__ Vector3 traceRayWithBVH(Ray ray, const unsigned int& root, const SceneMetaData& scenedata, float ior, Vector3 atten) {
	// This is the node index stack for traversing the BVH
	unsigned int nodestack[RECURSION_STACK_MAX_DEPTH];
	unsigned int idx = 0;
	// Push the root onto the top of the stack.
	nodestack[idx++] = root;
	// This is for keeping track of which instance is the closest!
	bool hit_instance = false;
	float t = FLT_MAX;
	Instance instance, instance_temp;
	// Enter loop through BVH - until the index is again 0, meaning all paths have been explored
	AABBnode node;
	while (idx > 0) {
		// Get the node from the previous place in the stack
		node = getAABBnode(nodestack[idx - 1]);
		// Done with this box
		idx--;
		// Only proceed if we hit the node
		if (node.hit(ray)) {
			// If this node is a leaf, we need to run intersection on the corresponding instance
			if (node.numChildren == 0) {
				instance_temp = getInstance(node.instance_id);
				float t_hit = instance_temp.hit(ray);
				// Check to see if we hit the enclosed instance
				if (t_hit > 0 && t_hit < t) {
					hit_instance = true;
					t = t_hit;
					instance = instance_temp;
				}
			}
			// If this node is not a leaf, we need to push its children onto the stack
			else {
				for (int n = 0; n < node.numChildren; n++) {
					nodestack[idx++] = node.firstChildIndex + n;
					if (idx >= RECURSION_STACK_MAX_DEPTH) return Vector3(1.0, 0.0, 0.0); // If for some reason the stack runs out, return RED
				}
			}
		}
	}
	// If we hit something, compute the appropriate color and send out some new rays
	if (hit_instance) {
		// Results go here
		Vector3 color = scenedata.clear_color;
		// Preliminary results that everybody needs
		Material material = getMaterial(instance.mat_id);
		Vector3 V = -ray.d;
		Vector3 point = ray.o + t * ray.d;
		// Get normal and texcoords
		Vector3 N(0.0);
		bool useTexcoord = false;
		Texcoord texcoord;
		if (instance.type == TRIANGLE) {
			Triangle triangle = getTriangle(instance.id);
			Transform tform = getTransform(instance.tform_id);
			Vector3 te1 = transformVector(tform, triangle.e1);
			Vector3 te2 = transformVector(tform, triangle.e2);
			Vector3 vn = cross(te1, te2);
			float A = norm(vn);
			N = vn / A;
			// Do we need barycentric coords?
			if (triangle.f.use_normals || triangle.f.use_texcoords) {
				// Barycentric interpolation
				Vector3 p1 = transformPoint(tform, getVertex(triangle.vert_id[0]).pos);
				Vector3 p2 = transformPoint(tform, getVertex(triangle.vert_id[1]).pos);
				Vector3 p3 = transformPoint(tform, getVertex(triangle.vert_id[2]).pos);
				float beta = dot(cross((point - p1), te2), N) / A;
				float gamma = dot(cross(te1, (point - p1)), N) / A;
				float alpha = 1 - beta - gamma;
				if (alpha < -HIT_EPSILON || beta < -HIT_EPSILON || gamma < -HIT_EPSILON || alpha > 1 + HIT_EPSILON || beta > 1 + HIT_EPSILON || gamma > 1 + HIT_EPSILON)
					N = Vector3(0.0);
				else {
					// Use normals in file
					if (triangle.f.use_normals) {
						Vector3 n1 = transformNormal(tform, getNormal(triangle.normal_id[0]).dir);
						Vector3 n2 = transformNormal(tform, getNormal(triangle.normal_id[1]).dir);
						Vector3 n3 = transformNormal(tform, getNormal(triangle.normal_id[2]).dir);
						N = alpha*n1 + beta*n2 + gamma*n3;
					}
					// Use texcoords in file
					if (triangle.f.use_texcoords) {
						useTexcoord = true;
						Texcoord tc1 = getTexcoord(triangle.texcoord_id[0]);
						Texcoord tc2 = getTexcoord(triangle.texcoord_id[1]);
						Texcoord tc3 = getTexcoord(triangle.texcoord_id[2]);
						texcoord.u = alpha*tc1.u + beta*tc2.u + gamma*tc3.u;
						texcoord.v = alpha*tc1.v + beta*tc2.v + gamma*tc3.v;
					}
				}
			}
		}
		else if (instance.type == SPHERE) {
			Sphere sphere = getSphere(instance.id);
			Transform tform = getTransform(instance.tform_id);
			N = normalize(point - transformPoint(tform, sphere.pos));
		}
		// Determine what coloring to use
		bool useDirect = false;
		bool useReflect = false;
		bool useRefract = false;
		bool useSchlick = false;
		// Outputs go here
		Vector3 directColor(0.0);
		Vector3 reflectColor(0.0);
		Vector3 refractColor(0.0);
		float Schlick = 0;
		// If this is a refractive material, do some fancy stuff /////////////////////////////
		if (material.n > 0) {
			bool exiting = dot(V, N) < 0;
			float n1 = ior;
			float n2 = exiting ? 1.0 : material.n;
			Vector3 atten1 = atten;
			Vector3 atten2 = exiting ? Vector3(0.0) : material.c;
			bool TIR = false;
			// Useful
			float c1 = -dot(ray.d, N);
			// Compute refract direction
			Ray refractray;
			refractray.o = exiting ? point + NORMAL_EPSILON * N : point - NORMAL_EPSILON * N;
			if (n2 == n1) refractray.d = ray.d;
			else {
				float eta = n1 / n2;
				float cs2 = 1 - (eta * eta * (1 - c1 * c1));
				if (cs2 < 0) TIR = true;
				else {
					refractray.d = eta * ray.d + (eta * c1 - sqrtf(cs2)) * N;
				}
			}
			// If complete reflection and entering ray - just phong shading and reflected ray
			if (TIR && !exiting) {
				useDirect = true;
				useReflect = true;
			}
			// If TIR and exiting ray - just launch the reflected ray
			else if (TIR && exiting) {
				useReflect = true;
			}
			// Non-trivial refraction and entering ray - use everything 
			else if (!TIR && !exiting) {
				useDirect = true;
				useReflect = true;
				useRefract = true;
				useSchlick = true;
			}
			// Leaving the medium - send only exiting ray
			else if (!TIR && exiting) {
				useRefract = true;
			}
			// Compute the things we need ////////////////
			// Schlick
			if (useSchlick) {
				float R0 = (n1 - n2) * (n1 - n2) / ((n1 + n2) * (n1 + n2));
				float negcos = 1 - c1;
				Schlick = R0 + (1 - R0)*negcos*negcos*negcos*negcos*negcos;
			}
			// Refract Color
			if (useRefract) {
				float factor = useSchlick ? 1 - Schlick : 1.0;
				refractray.dinv = 1 / refractray.d;
				refractray.sign[0] = refractray.dinv.x < 0;
				refractray.sign[1] = refractray.dinv.y < 0;
				refractray.sign[2] = refractray.dinv.z < 0;
				refractColor = clamp(traceRayWithBVH<depth + 1>(refractray, root, scenedata, n2, atten2));
				refractColor = factor * refractColor;
			}
			// Reflect Color
			if (useReflect) {
				float factor = useSchlick ? Schlick : 1.0;
				Ray reflectray;
				reflectray.o = exiting ? point - HIT_EPSILON * N : point + HIT_EPSILON * N;;
				reflectray.d = 2 * c1 * N + ray.d;
				reflectray.dinv = 1 / reflectray.d;
				reflectray.sign[0] = reflectray.dinv.x < 0;
				reflectray.sign[1] = reflectray.dinv.y < 0;
				reflectray.sign[2] = reflectray.dinv.z < 0;
				reflectColor = clamp(traceRayWithBVH<depth + 1>(reflectray, root, scenedata, n1, atten1)); // Reflection means remaining in the same medium.
				reflectColor = factor * reflectColor;
			}
		}
		else {
			// No special stuff, only direct lighting
			useDirect = true;
		}
		// Direct Lighting
		if (useDirect) {
			Vector3 texcolor(1.0);
			bool useColorTex = useTexcoord && instance.useColorTex;
			if (useColorTex) texcolor = imageTexFetch2D(instance.color_tex_id, texcoord.u, texcoord.v);
			// Ambient light
			if (scenedata.ambient_on) {
				//Vector3 ka = instance.useColorTex ? instance.color_tex_id;
				directColor += material.ka * texcolor * scenedata.ambient_light;
			}
			// Check for occlusion with all light sources
			for (int n = 0; n < scenedata.N_lights; n++) {
				Light light = getLight(n);
				Vector3 L = normalize(light.pos - point);
				// Check for inside cone (if directional) 
				if ((light.cos_theta > -1) && dot(-L, light.dir) < light.cos_theta) return color;
				// Check occlusion
				if (PointsHaveVisibility(point + NORMAL_EPSILON * N, light.pos, root, scenedata)) {
					// Diffuse light
					if (scenedata.diffuse_on) {
						directColor += clamp(fabs(dot(N, L)) * material.kd * texcolor * light.col);
					}
					// Specular light
					if (scenedata.specular_on) {
						directColor += pow(dot((2 * dot(L, N) * N - L), V), material.Ns) * material.ks * texcolor * light.col;
					}
				}
			}
			//directColor = Vector3(1, 0, 0);
		}
		// 4. Combine everything appropriately
		if (useDirect && !useReflect && !useRefract) {
			color = directColor;
		} 
		else {
			color = clamp((1 - material.kr) * directColor + material.kr * reflectColor + refractColor);
		}
		// Attenuate if necessary
		if (atten.x > 0 || atten.y > 0 || atten.z > 0) {
			Vector3 attenuation;
			attenuation.x = __expf(-t * atten.x);
			attenuation.y = __expf(-t * atten.y);
			attenuation.z = __expf(-t * atten.z);
			color *= attenuation;
		}
		// Return when done
		return color;
	}
	// Otherwise return the clear color
	return scenedata.clear_color;
}
// End recursion at too many rays, or BVH returned no hit
template<>
__device__ Vector3 traceRayWithBVH<MAX_RAY_DEPTH>(Ray ray, const unsigned int& root, const SceneMetaData& scenedata, float ior, Vector3 atten) {
	// Return clear color
	return scenedata.clear_color;
}

// Raytrace without the BVH
template <unsigned int depth>
__device__ Vector3 traceRayNoBVH(const Ray& ray, const unsigned int& root, const SceneMetaData& scenedata) {
	// Check if we hit the scene
	AABBnode node = getAABBnode(root);
	if (node.hit(ray)) {
		Instance instance_closest;
		bool hit_instance = false;
		float t_closest = FLT_MAX;
		// Look at the bounding box for every instance
		for (int n = 0; n < scenedata.N_instances; n++) {
			node = getAABBnode(n);
			if (node.hit(ray)) {
				Instance instance = getInstance(node.instance_id);
				float t_hit = instance.hit(ray);
				if (t_hit > 0 && t_hit < t_closest) {
					hit_instance = true;
					instance_closest = instance;
					t_closest = t_hit;
				}
			}
		}
		// Compute the color and dispatch subsequent rays if we hit it
		if (hit_instance) {
			Vector3 color = Vector3(1.0, 1.0, 1.0);
			return color;
		}
	}
	// No dice
	return scenedata.clear_color;
}
// End recursion
template<>
__device__ Vector3 traceRayNoBVH<MAX_RAY_DEPTH>(const Ray& ray, const unsigned int& root, const SceneMetaData& scenedata) {
	// Return clear color
	return scenedata.clear_color;
}
// ----------------------------------------------------------------------------
// Kernels
// ----------------------------------------------------------------------------
// 
#define MAX_THREADS 256
// Simply calculates all AABBs for all instances and puts them in linear memory
__global__ void NoBVHGetInstanceAABBs(AABBnode* bvhmem, unsigned int N_instances) {
	int thread = blockIdx.x * blockDim.x + threadIdx.x;
	if (thread < N_instances) {
		// Get the instance for this thread
		Instance i = getInstance(thread);
		// Get the AABB and store it in the appropriate place
		AABBnode aabb = i.getAABB();
		aabb.instance_id = thread; // this must point to the index of the instance!!!
		aabb.numChildren = 0; // no children, this is a leaf
		// Store in the BVH
		bvhmem[thread] = aabb;
	}
}

// Shifts all AABBs over by #instances, also fills in cpu array with morton codes properly
#define SCALE_FACTOR 100
__global__ void GetMortonCodes(AABBnode* bvhmem, cpuAABB* dev_cpuaabbs, unsigned int N_instances) {
	unsigned int thread = blockIdx.x * blockDim.x + threadIdx.x;
	if (thread < N_instances) {
		Instance inst = getInstance(thread);
		int a = SCALE_FACTOR * inst.centroid.x;
		int b = SCALE_FACTOR * inst.centroid.y;
		int c = SCALE_FACTOR * inst.centroid.z;
		// Make sure we have enough that
		UINT64 code = 0;
		// Least-significant bits Morton ordering - 21 bits each
		for (int i = 0; i < (sizeof(UINT64) * CHAR_BIT) / 3; i++) {
			code |= ((a & ((UINT64)1 << i)) << (3 * i)) | ((b & ((UINT64)1 << i)) << (3 * i + 1)) | ((c & ((UINT64)1 << i)) << (3*i + 2));
		}
		// Get the actual aabb and store it in the second half of memory
		AABBnode aabb = inst.getAABB();
		aabb.instance_id = thread;
		aabb.numChildren = 0;
		bvhmem[N_instances + thread] = aabb;
		// Put the code in the cpu array
		dev_cpuaabbs[thread].mcode = code;
		dev_cpuaabbs[thread].sort_id = N_instances + thread;
	}
}

// Puts all the AABBs back at the beginning, but this time ordered properly by morton code
__global__ void ReorderAABBsOnGPU(AABBnode* bvhmem, cpuAABB* dev_cpuaabbs, unsigned int N_instances) {
	int thread = blockIdx.x * blockDim.x + threadIdx.x;
	if (thread < N_instances) {
		cpuAABB cpuaabb = dev_cpuaabbs[thread];
		AABBnode aabb = bvhmem[cpuaabb.sort_id];
		bvhmem[thread] = aabb;
	}
}

// This kernel build the BVH for a generic k-ary tree structure -- IMPLEMENTED ONLY FOR BINARY RIGHT NOW
__global__ void TreeLevelBVHKernel(AABBnode* bvhmem, unsigned int firstInLevel, unsigned int numInLevel, unsigned int firstInChildLevel, unsigned int numInChildLevel) {
	unsigned int thread = blockIdx.x * blockDim.x + threadIdx.x;
	if (thread < numInLevel) {
		// Can only have a single-child node if the child level has an odd number of nodes and this is the last node in the level
		unsigned int numChildren = ((thread == numInLevel - 1) && (numInChildLevel % 2 == 1)) ? 1 : 2;
		// This is the pointer to the start of the children
		unsigned int firstChildIndex = firstInChildLevel + 2 * thread;
		// Build the new node
		AABBnode aabb;
		// If there are two children, the bounding volume must enclose both children
		if (numChildren == 2) {
			AABBnode c1 = bvhmem[firstChildIndex];
			AABBnode c2 = bvhmem[firstChildIndex + 1];
			aabb.upperFrontRight.x = c1.upperFrontRight.x > c2.upperFrontRight.x ? c1.upperFrontRight.x : c2.upperFrontRight.x;
			aabb.upperFrontRight.y = c1.upperFrontRight.y > c2.upperFrontRight.y ? c1.upperFrontRight.y : c2.upperFrontRight.y;
			aabb.upperFrontRight.z = c1.upperFrontRight.z > c2.upperFrontRight.z ? c1.upperFrontRight.z : c2.upperFrontRight.z;
			aabb.lowerBackLeft.x = c1.lowerBackLeft.x < c2.lowerBackLeft.x ? c1.lowerBackLeft.x : c2.lowerBackLeft.x;
			aabb.lowerBackLeft.y = c1.lowerBackLeft.y < c2.lowerBackLeft.y ? c1.lowerBackLeft.y : c2.lowerBackLeft.y;
			aabb.lowerBackLeft.z = c1.lowerBackLeft.z < c2.lowerBackLeft.z ? c1.lowerBackLeft.z : c2.lowerBackLeft.z;
			aabb.numChildren = numChildren;
			aabb.firstChildIndex = firstChildIndex;
		}
		// If there is one child, we want to just copy the attributes of the child
		else {
			AABBnode c1 = bvhmem[firstChildIndex];
			aabb = c1;
		}
		// Put the new node where it belongs in memory
		bvhmem[firstInLevel + thread] = aabb;
	}
}


// This kernel builds the BVH for the uniform grid acceleration structure
__global__ void UniformBVHKernel(SceneMetaData &scenedata) {
	int thread = blockIdx.x * blockDim.x + threadIdx.x;
	// Get the instance
	Instance i = getInstance(thread);
}

// Clears the pixels
__global__ void ClearKernel(float4* pixels, unsigned int resx, unsigned int resy, Vector3 clear_color) {
	unsigned int id = blockIdx.x * blockDim.x + threadIdx.x;
	unsigned int width = resx;
	unsigned int height = resy;
	// This is where intermediate pixel results will be stored
	if (id < width * height) {
		pixels[id].x = clear_color.x;
		pixels[id].y = clear_color.y;
		pixels[id].z = clear_color.z;
	}
}

// Scales the pixels
__global__ void ScaleKernel(float4* pixels, unsigned int resx, unsigned int resy, float scale) {
	unsigned int id = blockIdx.x * blockDim.x + threadIdx.x;
	unsigned int width = resx;
	unsigned int height = resy;
	// This is where intermediate pixel results will be stored
	if (id < width * height) {
		pixels[id].x *= scale;
		pixels[id].y *= scale;
		pixels[id].z *= scale;
	}
}


// Basic kernel for ray tracing
__global__ void DrawKernel(float4* pixels, unsigned int bvhroot, SceneMetaData scenedata) {
	unsigned int id = blockIdx.x * blockDim.x + threadIdx.x;
	unsigned int width = scenedata.resx;
	unsigned int height = scenedata.resy;
	// This is where intermediate pixel results will be stored
	if (id < width * height) {
		// Screen space coordinates
		float sx = -1.0 + 2 * ((id % width)/(float)(width));
		float sy = 1.0 - 2 * ((id / width)/(float)(width));
		// Get the ray for this pixel
		Ray ray = getRayThroughPixel(sx, sy, scenedata.camera);
		// Off on your way, ray!!!
		Vector3 color;
		if (scenedata.useBVH) {
			color = traceRayWithBVH<0>(ray, bvhroot, scenedata, 1.0, Vector3(0.0));
		}
		else {
			// This is working!!!
			color = traceRayNoBVH<0>(ray, bvhroot, scenedata);
		}
		pixels[id].x += color.x;
		pixels[id].y += color.y;
		pixels[id].z += color.z;
	}
}

// Smart anti-aliasing
// Maximum norm of pixel difference allowed before AA kicks in:
#define AA_THRESHOLD 0.1
__global__ void AntiAliasKernel(float4* pixels, unsigned int bvhroot, SceneMetaData scenedata) {
	unsigned int id = blockIdx.x * blockDim.x + threadIdx.x;
	unsigned int width = scenedata.resx;
	unsigned int height = scenedata.resy;
	unsigned int aarate = scenedata.antialiasrate;
	// This is where intermediate pixel results will be stored
	__shared__ Vector3 temp_pixels[MAX_THREADS]; // There will never be more than MAX_THREADS pixels in a thread...
	__syncthreads();
	if (id < width * height * aarate * aarate) {
		// Figure out which pixel this is
		unsigned int pix = id / (aarate * aarate);
		// Find x,y of pixel
		unsigned int pixx = pix % width;
		unsigned int pixy = pix / width;
		// Before doing anything else, check it is even worth antialiasing this pixel...
		Vector3 mypixel = Vector3(pixels[pix].x, pixels[pix].y, pixels[pix].z);
		Vector3 pixel;
		float diff;
		bool needToDoAA = false;
		for (int i = -3; i <= 3; i++) {
			// Check horizontally
			if (pixx >= 3 && pixx < width - 3) {
				pixel = Vector3(pixels[pix + i].x, pixels[pix + i].y, pixels[pix + i].z);
				diff = norm(mypixel - pixel);
				if (diff > AA_THRESHOLD) {
					needToDoAA = true;
					break;
				}
			}
			// Check vertically
			if (pixy >= 3 && pixy < height - 3) {
				pixel = Vector3(pixels[pix + i*width].x, pixels[pix + i*width].y, pixels[pix + i*width].z);
				diff = norm(mypixel - pixel);
				if (diff > AA_THRESHOLD) {
					needToDoAA = true;
					break;
				}
			}
		}
		// This thread can stop here if we don't need to do AA
		if (!needToDoAA) return;

		// Otherwise, continue...
		unsigned int subpix = id % (aarate * aarate);
		bool iAmABoss = subpix == 0;
		unsigned int subpixx = subpix % aarate;
		unsigned int subpixy = subpix / aarate;

		// Screen space coordinates
		float sx = -1.0 + 2 * ((pixx * aarate + subpixx) / (float)(aarate * width));
		float sy = 1.0 - 2 * ((pixy * aarate + subpixy) / (float)(aarate * width));
		// Get the ray for this pixel
		Ray ray = getRayThroughPixel(sx, sy, scenedata.camera);
		// Off on your way, ray!!!
		Vector3 color;
		if (scenedata.useBVH) {
			color = traceRayWithBVH<0>(ray, bvhroot, scenedata, 1.0, Vector3(0.0));
		}
		else {
			// This is working!!!
			color = traceRayNoBVH<0>(ray, bvhroot, scenedata);
		}
		// Load the color into the pixel
		temp_pixels[threadIdx.x] = color;
		// synchronize
		__syncthreads();
		Vector3 combinedColor(0.0);
		// Average over antialias values
		if (iAmABoss) {
			for (int i = 0; i < aarate * aarate; i++) {
				combinedColor += temp_pixels[threadIdx.x + i];
			}
			combinedColor /= (aarate * aarate);
			pixels[pix].x += combinedColor.x - temp_pixels[threadIdx.x].x; // Subtract out the contribution from the original call to DrawKernel<<<>>> (thank you determinism...)
			pixels[pix].y += combinedColor.y - temp_pixels[threadIdx.x].y;
			pixels[pix].z += combinedColor.z - temp_pixels[threadIdx.x].z;
		}
	}
}

// For testing
//__global__ void TestKernel(float4* pixels, unsigned int bvhroot, SceneMetaData scenedata) {
//	int id = blockIdx.x * blockDim.x + threadIdx.x;
//	int width = scenedata.resx;
//	int height = scenedata.resy;
//	Vector3 red(1.0f, 0.0f, 0.0f);
//	Vector3 green(0.0f, 1.0f, 0.0f);
//	Vector3 blue(0.0f, 0.0f, 1.0f);
//	Vector3 gray(0.4f, 0.4f, 0.4f);
//	Vector3 white(1.0);
//	if (id < scenedata.N_bvhnodes) {
//		// TESTS //
//		Vector3 color(0.0);
//		AABBnode aabb = getAABBnode(id);
//		if (aabb.numChildren == 0) {
//			Instance instance = getInstance(aabb.instance_id);
//			if (instance.type == SPHERE) {
//				Sphere sphere = getSphere(instance.id);
//				color = sphere.pos;
//			}
//			else if (instance.type == TRIANGLE) {
//				Triangle triangle = getTriangle(instance.id);
//				if (triangle.e1.z < 0) color = blue;
//				else color = green;
//			}
//		}
//
//
//		//// Test the lights
//		//for (int n = 0; n < scenedata.N_lights; n++) {
//		//	Light light = getLight(n);
//		//	color = light.col;
//		//}
//		//// Test the materials
//		//for (int n = 0; n < scenedata.N_materials; n++) {
//		//	Material material = getMaterial(n);
//		//	color = material.ka;
//		//}
//		//// Test the transforms
//		//for (int n = 0; n < scenedata.N_transforms; n++) {
//		//	Transform transform = getTransform(n);
//		//	if (transform.A[0][0] == 1) color = white;
//		//}
//		//// Test the vertices
//		//for (int n = 0; n < scenedata.N_vertices; n++) {
//		//	Vertex vertex = getVertex(n);
//		//	color = vertex.pos;
//		//}
//		//// Test the triangles
//		//for (int n = 0; n < scenedata.N_triangles; n++) {
//		//	Triangle triangle = getTriangle(n);
//		//	color = triangle.e2;
//		//}
//		//// Test the spheres
//		//for (int n = 0; n < scenedata.N_spheres; n++) {
//		//	Sphere sphere = getSphere(n);
//		//	color = sphere.pos;
//		//}
//		//// Test the instances
//		//for (int n = 0; n < scenedata.N_instances; n++) {
//		//	Instance instance = getInstance(n);
//		//	color = instance.centroid;
//		//}
//		//// Test the boxes
//		//for (int n = 0; n < scenedata.N_boxes; n++) {
//		//	Box box = getBox(n);
//		//	if (box.vert_id[0] == 0) color = white;
//		//}
//		//// Test the BVH
//		//for (int n = 0; n < scenedata.N_bvhnodes; n++) {
//		//	AABBnode aabb = getAABBnode(n);
//		//	color = aabb.lowerBackLeft;
//		//}
//
//
//
//
//		pixels[id].x = color.x;
//		pixels[id].y = color.y;
//		pixels[id].z = color.z;
//	}
//}

///////////////////////////////////////////////////////////////////////////////////////////////
//-------------------------------------------------------------------------------------------//
//-------------------------------------------------------------------------------------------//
//								Externally Visible									         //
//-------------------------------------------------------------------------------------------//
//-------------------------------------------------------------------------------------------//
///////////////////////////////////////////////////////////////////////////////////////////////
// This is how many reps are done for DOF runs
#define DOF_REPS 60

// This is called from the outside world to invoke the rendering process
extern "C" void runKernels(float4* pixels, SceneMetaData scenedata) {
	// First load the textures in
	SetupTextures(scenedata);
	// Run the appropriate kernel based on the BVH mode
	unsigned int root;
	if (scenedata.bvhmode == BVHMODE_TREE) {
		// Morton order the bounding boxes
		MortonOrderAABBs(scenedata);
		root = SetupTreeBVH(scenedata);
	}
	else {
		// Going to prepare only the most basic form of BVH
		root = SetupSceneBoundingVolumeOnly(scenedata);
	}
	// Load the BVH into texture memory
	cudaChannelFormatDesc floatdesc = cudaCreateChannelDesc<float>();
	if (scenedata.N_bvhnodes > 0) HANDLE_ERROR(cudaBindTexture(NULL, &BVH_TEX, scenedata.bvhmem, &floatdesc, scenedata.N_bvhnodes * sizeof(AABBnode)));
	// Now can run the raytracer
	CheckAllGPUMemory(scenedata); // For Debug purposes - will print contents of gpu memory to text file.
	// Clear all pixels
	int totalthreads = scenedata.resx * scenedata.resy;
	int threaddim = 256;
	int blockdim = (int)ceil(totalthreads / (float)threaddim);
	ClearKernel <<< blockdim, threaddim >>>(pixels, scenedata.resx, scenedata.resy, scenedata.clear_color);
	HANDLE_ERROR(cudaGetLastError());
	// At the end of all processing, the image will be scaled by this factor
	float scale = 1.0f;
	// DOF effect taken into account here
	int numReps = 1;
	if (scenedata.camera.aperture > 0) {
		numReps = DOF_REPS;
		scale *= numReps;
	}
	cout << "Ray tracing: " << "DOF = " << numReps << "x\t AA = " << scenedata.antialiasrate << "x" << endl;
	// Repeat to obtain DOF effect
	// Same seed so effect is consistent...
	srand(1);
	for (int rep = 0; rep < numReps; rep++) {
		// Adjust camera eye location randomly inside aperture
		if (rep != 0) {
			Vector3 direction = host_normalize(scenedata.camera.look - scenedata.camera.eye);
			Vector3 dx = host_normalize(host_cross(direction, scenedata.camera.up));
			Vector3 dy = host_normalize(host_cross(dx, direction));
			float r = scenedata.camera.aperture * (rand() / (float)RAND_MAX);
			float theta = (float)2*M_PI*(rand() / (float)RAND_MAX);
			scenedata.camera.eyeoffset = r * (cos(theta)*dx + sin(theta)*dy);
		}
		// Ray trace
		totalthreads = scenedata.resx * scenedata.resy;
		threaddim = 256;
		blockdim = (int)ceil(totalthreads / (float)threaddim);
		//cout << "Launching initial RayTrace kernel <<<" << blockdim << ", " << threaddim << ">>>..." << endl;
		DrawKernel <<< blockdim, threaddim >>>(pixels, root, scenedata);
		HANDLE_ERROR(cudaGetLastError());
		// If antialising is enabled, invoke the anti-alias kernel
		if (scenedata.antialiasrate > 1) {
			totalthreads = scenedata.resx * scenedata.resy * scenedata.antialiasrate * scenedata.antialiasrate;
			threaddim = 256 * scenedata.antialiasrate * scenedata.antialiasrate;
			while (threaddim > MAX_THREADS) threaddim /= 2;
			blockdim = (int)ceil(totalthreads / (float)threaddim);
			//cout << "Launching Anti-Alias kernel <<<" << blockdim << ", " << threaddim << ">>>..." << endl;
			AntiAliasKernel <<< blockdim, threaddim >>>(pixels, root, scenedata);
			HANDLE_ERROR(cudaGetLastError());
		}
		// Synchronize before repeating
		HANDLE_ERROR(cudaDeviceSynchronize());
	}
	// Scale the composited image by the number of repetitions ( if necessary) 
	if (scale != 1) {
		totalthreads = scenedata.resx * scenedata.resy;
		threaddim = 256;
		blockdim = (int)ceil(totalthreads / (float)threaddim);
		ScaleKernel <<<blockdim, threaddim >>>(pixels, scenedata.resx, scenedata.resy, 1/scale);
		HANDLE_ERROR(cudaGetLastError());
	}
	// Synchronize before moving on
	HANDLE_ERROR(cudaDeviceSynchronize());
	// Unbind when done
	UnbindTextures();
}

///////////////////////////////////////////////////////////////////////////////////////////////
//-------------------------------------------------------------------------------------------//
//-------------------------------------------------------------------------------------------//
//								HELPER FUNCTIONS									         //
//-------------------------------------------------------------------------------------------//
//-------------------------------------------------------------------------------------------//
///////////////////////////////////////////////////////////////////////////////////////////////

// For debugging
void CheckAllGPUMemory(SceneMetaData scenedata) {
	// Start the file output
	ofstream myfile;
	myfile.open("../Debug/gpu-mem-dump.txt");

	// All memory switcheroos...
	// PixelArrays
	// lights
	HANDLE_ERROR(cudaUnbindTexture(&LIGHTS_TEX));
	Light* cpulightmem = (Light*)malloc(scenedata.N_lights * sizeof(Light));
	HANDLE_ERROR(cudaMemcpy(cpulightmem, scenedata.lights, scenedata.N_lights * sizeof(Light), cudaMemcpyDeviceToHost));
	// Normals
	HANDLE_ERROR(cudaUnbindTexture(&NORMALS_TEX));
	Normal* cpunormalmem = (Normal*)malloc(scenedata.N_normals * sizeof(Normal));
	HANDLE_ERROR(cudaMemcpy(cpunormalmem, scenedata.normals, scenedata.N_normals * sizeof(Normal), cudaMemcpyDeviceToHost));
	// Texcoords
	HANDLE_ERROR(cudaUnbindTexture(&TEXCOORDS_TEX));
	Texcoord* cputexcoordmem = (Texcoord*)malloc(scenedata.N_texcoords * sizeof(Texcoord));
	HANDLE_ERROR(cudaMemcpy(cputexcoordmem, scenedata.texcoords, scenedata.N_texcoords * sizeof(Texcoord), cudaMemcpyDeviceToHost));
	// vertices
	HANDLE_ERROR(cudaUnbindTexture(&VERTICES_TEX));
	Vertex* cpuvertexmem = (Vertex*)malloc(scenedata.N_vertices * sizeof(Vertex));
	HANDLE_ERROR(cudaMemcpy(cpuvertexmem, scenedata.vertices, scenedata.N_vertices * sizeof(Vertex), cudaMemcpyDeviceToHost));
	// materials
	HANDLE_ERROR(cudaUnbindTexture(&MATERIALS_TEX));
	Material* cpumaterialmem = (Material*)malloc(scenedata.N_materials * sizeof(Material));
	HANDLE_ERROR(cudaMemcpy(cpumaterialmem, scenedata.materials, scenedata.N_materials * sizeof(Material), cudaMemcpyDeviceToHost));
	// transforms
	HANDLE_ERROR(cudaUnbindTexture(&TRANSFORMS_TEX));
	Transform* cputransformmem = (Transform*)malloc(scenedata.N_transforms * sizeof(Transform));
	HANDLE_ERROR(cudaMemcpy(cputransformmem, scenedata.transforms, scenedata.N_transforms * sizeof(Transform), cudaMemcpyDeviceToHost));
	// instances
	HANDLE_ERROR(cudaUnbindTexture(&INSTANCES_TEX));
	Instance* cpuinstancemem = (Instance *)malloc(scenedata.N_instances * sizeof(Instance));
	HANDLE_ERROR(cudaMemcpy(cpuinstancemem, scenedata.instances, scenedata.N_instances * sizeof(Instance), cudaMemcpyDeviceToHost));
	// triangles
	HANDLE_ERROR(cudaUnbindTexture(&TRIANGLES_TEX));
	Triangle* cputrianglemem = (Triangle*)malloc(scenedata.N_triangles * sizeof(Triangle));
	HANDLE_ERROR(cudaMemcpy(cputrianglemem, scenedata.triangles, scenedata.N_triangles * sizeof(Triangle), cudaMemcpyDeviceToHost));
	// spheres
	HANDLE_ERROR(cudaUnbindTexture(&SPHERES_TEX));
	Sphere* cpuspheremem = (Sphere*)malloc(scenedata.N_spheres * sizeof(Sphere));
	HANDLE_ERROR(cudaMemcpy(cpuspheremem, scenedata.spheres, scenedata.N_spheres * sizeof(Sphere), cudaMemcpyDeviceToHost));
	// boxes
	HANDLE_ERROR(cudaUnbindTexture(&BOXES_TEX));
	Box* cpuboxmem = (Box*)malloc(scenedata.N_boxes * sizeof(Box));
	HANDLE_ERROR(cudaMemcpy(cpuboxmem, scenedata.boxes, scenedata.N_boxes * sizeof(Box), cudaMemcpyDeviceToHost));
	// bvh
	AABBnode* cpubvhmem = (AABBnode*)malloc(scenedata.N_bvhnodes * sizeof(AABBnode));
	HANDLE_ERROR(cudaMemcpy(cpubvhmem, scenedata.bvhmem, scenedata.N_bvhnodes * sizeof(AABBnode), cudaMemcpyDeviceToHost));


	// Test PixelArrays memory ///////////////////////////////////////////////////
	myfile << "\n\n--------- Image Textures ---------- \n\n";
	for (unsigned int n = 0; n < scenedata.N_images; n++) {
		CRImage pixarr = scenedata.images[n];
		myfile << "Image " << n << ":\twidth = " << pixarr.width << "\t--\theight = " << pixarr.height << "\t--\tpitch = " << pixarr.pitch << endl;
	}

	// Test Lights memory ////////////////////////////////////////////////////////
	myfile << "\n\n---------- Lights ----------\n\n";
	for (unsigned int n = 0; n < scenedata.N_lights; n++) {
		Light light = cpulightmem[n];
		myfile << "<" << light.pos.x << ", " << light.pos.y << ", " << light.pos.z << ">\t";
		myfile << "<" << light.col.x << ", " << light.col.y << ", " << light.col.z << ">\t";
		myfile << "<" << light.dir.x << ", " << light.dir.y << ", " << light.dir.z << ">\t";
		myfile << "cos_theta = " << light.cos_theta << endl;
	}
	// Test Materials memory ////////////////////////////////////////////////////////
	myfile << "\n\n---------- MATERIALS ----------\n\n";
	for (unsigned int n = 0; n < scenedata.N_materials; n++) {
		//Material material = cpumaterialmem[n];
		myfile << "Material " << n << endl;
	}
	// Test Transforms memory ////////////////////////////////////////////////////////
	myfile << "\n\n---------- Transforms ----------\n\n";
	for (unsigned int n = 0; n < scenedata.N_transforms; n++) {
		Transform transform = cputransformmem[n];
		for (int i = 0; i < 4; i++)
			for (int j = 0; j < 4; j++)
				myfile << transform.A[i][j] << ", ";
		myfile << endl;
		for (int i = 0; i < 4; i++)
			for (int j = 0; j < 4; j++)
				myfile << transform.AinvT[i][j] << ", ";
		myfile << endl;
		myfile << endl;
	}
	// Test Normals memory ////////////////////////////////////////////////////////
	myfile << "\n\n---------- Normals ----------\n\n";
	for (unsigned int n = 0; n < scenedata.N_normals; n++) {
		Normal normal = cpunormalmem[n];
		myfile << "<" << normal.dir.x << ", " << normal.dir.y << ", " << normal.dir.z << ">" << endl;
	}
	myfile << "\n\n---------- Texcoords ----------\n\n";
	for (unsigned int n = 0; n < scenedata.N_texcoords; n++) {
		Texcoord texcoord = cputexcoordmem[n];
		myfile << "<" << texcoord.u << ", " << texcoord.v <<  ">" << endl;
	}
	// Test Vertices memory ////////////////////////////////////////////////////////
	myfile << "\n\n---------- Vertices ----------\n\n";
	for (unsigned int n = 0; n < scenedata.N_vertices; n++) {
		Vertex vertex = cpuvertexmem[n];
		myfile << "<" << vertex.pos.x << ", " << vertex.pos.y << ", " << vertex.pos.z << ">" << endl;
	}
	// Test Triangles memory ////////////////////////////////////////////////////////
	myfile << "\n\n---------- Triangles ----------\n\n";
	for (unsigned int n = 0; n < scenedata.N_triangles; n++) {
		Triangle triangle = cputrianglemem[n];
		myfile << "vert_ids: [" << triangle.vert_id[0] << ", " << triangle.vert_id[1] << ", " << triangle.vert_id[2] << "]";
		myfile << "normal_ids: [" << triangle.normal_id[0] << ", " << triangle.normal_id[1] << ", " << triangle.normal_id[2] << "]";
		myfile << "texcoord_ids: [" << triangle.texcoord_id[0] << ", " << triangle.texcoord_id[1] << ", " << triangle.texcoord_id[2] << "]";
		myfile << "\tuse_normals: " << triangle.f.use_normals << "\tuse_texcoords: " << triangle.f.use_texcoords;
		myfile << "\te1: <" << triangle.e1.x << ", " << triangle.e1.y << ", " << triangle.e1.z << ">";
		myfile << "\te2: <" << triangle.e2.x << ", " << triangle.e2.y << ", " << triangle.e2.z << ">" << endl;
	}
	// Test Spheres memory ////////////////////////////////////////////////////////
	myfile << "\n\n---------- Spheres ----------\n\n";
	for (unsigned int n = 0; n < scenedata.N_spheres; n++) {
		Sphere sphere = cpuspheremem[n];
		myfile << "pos: <" << sphere.pos.x << ", " << sphere.pos.y << ", " << sphere.pos.z << ">";
		myfile << "\tr: " << sphere.r << endl;
	}
	// Test Instances memory ////////////////////////////////////////////////////////
	myfile << "\n\n---------- Instances ----------\n\n";
	for (unsigned int n = 0; n < scenedata.N_instances; n++) {
		Instance instance = cpuinstancemem[n];
		myfile << "type: " << instance.type << "\tid: " << instance.id;
		myfile << "\ttform_id: " << instance.tform_id << "\tmat_id: " << instance.mat_id;
		myfile << "\tcolor_tex_id: " << instance.color_tex_id << "\tuseColorTex: " << instance.useColorTex;
		myfile << "\tnormal_tex_id: " << instance.normal_tex_id << "\tuseNormalTex: " << instance.useNormalTex;
		myfile << "\tcentroid: <" << instance.centroid.x << ", " << instance.centroid.y << ", " << instance.centroid.z << ">" << endl;
	}
	// Test Boxes memory ////////////////////////////////////////////////////////
	myfile << "\n\n---------- Boxes ----------\n\n";
	for (unsigned int n = 0; n < scenedata.N_boxes; n++) {
		myfile << "Box " << n << endl;
	}
	// Test BVH memory ////////////////////////////////////////////////////////
	myfile << "\n\n---------- BVH ----------\n\n";
	for (unsigned int n = 0; n < scenedata.N_bvhnodes; n++)	 {
		if (n == scenedata.N_instances) myfile << "-------------------------------" << endl;
		AABBnode aabb = cpubvhmem[n];
		myfile << n << ":\tfirstChild: " << aabb.firstChildIndex << " \t -- \t numChildren: " << aabb.numChildren << endl;
	}

	// Free all the CPU memory and rebind textures
	free(cpulightmem);
	free(cpunormalmem);
	free(cpuvertexmem);
	free(cpumaterialmem);
	free(cputransformmem);
	free(cpuinstancemem);
	free(cputrianglemem);
	free(cpuspheremem);
	free(cpuboxmem);
	free(cpubvhmem);
	SetupTextures(scenedata);
	
	// Close the file
	myfile.close();
}

// Sets up the vanilla BVH with only a scene bounding volume, and a AABB for each instance
unsigned int SetupSceneBoundingVolumeOnly(SceneMetaData scenedata) {
	// Generate all of the instance AABBs
	NoBVHGetInstanceAABBs <<< (unsigned int)ceil(scenedata.N_instances / (float)256), 256 >>>(scenedata.bvhmem, scenedata.N_instances);
	HANDLE_ERROR(cudaGetLastError());
	// Just get the enclosing volume on the CPU
	AABBnode sceneaabb;
	sceneaabb.firstChildIndex = 0;
	sceneaabb.numChildren = scenedata.N_instances;
	sceneaabb.lowerBackLeft = Vector3(FLT_MAX, FLT_MAX, FLT_MAX);
	sceneaabb.upperFrontRight = Vector3(-FLT_MAX, -FLT_MAX, -FLT_MAX);
	AABBnode* cpuNodes = (AABBnode*)malloc(sizeof(AABBnode) * scenedata.N_instances);
	cudaMemcpy(cpuNodes, scenedata.bvhmem, sizeof(AABBnode) * scenedata.N_instances, cudaMemcpyDeviceToHost);
	for (unsigned int n = 0; n < scenedata.N_instances; n++) {
		AABBnode iaabb = cpuNodes[n];
		sceneaabb.lowerBackLeft.x = iaabb.lowerBackLeft.x < sceneaabb.lowerBackLeft.x ? iaabb.lowerBackLeft.x : sceneaabb.lowerBackLeft.x;
		sceneaabb.lowerBackLeft.y = iaabb.lowerBackLeft.y < sceneaabb.lowerBackLeft.y ? iaabb.lowerBackLeft.y : sceneaabb.lowerBackLeft.y;
		sceneaabb.lowerBackLeft.z = iaabb.lowerBackLeft.z < sceneaabb.lowerBackLeft.z ? iaabb.lowerBackLeft.z : sceneaabb.lowerBackLeft.z;
		sceneaabb.upperFrontRight.x = iaabb.upperFrontRight.x > sceneaabb.upperFrontRight.x ? iaabb.upperFrontRight.x : sceneaabb.upperFrontRight.x;
		sceneaabb.upperFrontRight.y = iaabb.upperFrontRight.y > sceneaabb.upperFrontRight.y ? iaabb.upperFrontRight.y : sceneaabb.upperFrontRight.y;
		sceneaabb.upperFrontRight.z = iaabb.upperFrontRight.z > sceneaabb.upperFrontRight.z ? iaabb.upperFrontRight.z : sceneaabb.upperFrontRight.z;
	}
	// Stick it in the right spot
	cudaMemcpy(&scenedata.bvhmem[scenedata.N_instances], &sceneaabb, sizeof(AABBnode), cudaMemcpyHostToDevice);
	// Done
	free(cpuNodes);
	// Update the root variable
	return scenedata.N_instances;
}

// Assumes that the AABBs have been Morton-ordered - builds binary tree structure
unsigned int SetupTreeBVH(SceneMetaData scenedata) {
	// Build the tree one level at a time. (somewhat expensive, kernel launch per level)
	unsigned int height = scenedata.N_bvhmaxdepth - 1;
	unsigned int N_instances = scenedata.N_instances;
	// First get some useful info
	unsigned int nodesInLevel[32]; // Hopefully 32 >= height+1 is always true...
	unsigned int firstNodeInLevel[32];
	// First calculate the number of nodes in every level and where each level starts
	unsigned int N = N_instances;
	unsigned int cumsum = 0;
	for (int n = height; n >= 0; n--) {
		nodesInLevel[n] = N;
		firstNodeInLevel[n] = cumsum;
		cumsum += N;
		N = (N + 1) / 2;
	}
	// Bottom up building, launch the kernel for each level
	for (int level = height - 1; level >= 0; level--) {
		int totalthreads = nodesInLevel[level];
		int threaddim = 32;
		int blockdim = (int)ceil(totalthreads / (float)threaddim);
		TreeLevelBVHKernel <<< blockdim, threaddim >>>(scenedata.bvhmem, firstNodeInLevel[level], nodesInLevel[level], firstNodeInLevel[level+1], nodesInLevel[level+1]);
	}
	return scenedata.N_bvhnodes - 1; // The root will be the very last node
}

// Combination of GPU, CPU sorting to get the AABBs ordered and back in the correct spot in GPU memory
void MortonOrderAABBs(SceneMetaData scenedata) {
	// Make some GPU memory to work with
	cpuAABB* dev_cpuaabbs;
	HANDLE_ERROR(cudaMalloc((void**)&dev_cpuaabbs, sizeof(cpuAABB) * scenedata.N_instances));
	// Now get Morton codes for every instance.
	GetMortonCodes <<<(unsigned int)ceil(scenedata.N_instances / (float)256), 256 >>>(scenedata.bvhmem, dev_cpuaabbs, scenedata.N_instances);
	HANDLE_ERROR(cudaGetLastError());
	// Copy that memory to the CPU
	cpuAABB* cpuaabbs = (cpuAABB*)malloc(sizeof(cpuAABB) * scenedata.N_instances);
	HANDLE_ERROR(cudaMemcpy(cpuaabbs, dev_cpuaabbs, sizeof(cpuAABB) * scenedata.N_instances, cudaMemcpyDeviceToHost));
	// Make a vector with the data
	vector<cpuAABB> cpuaabbsvec;
	cpuaabbsvec.reserve(scenedata.N_instances);
	for (unsigned int n = 0; n < scenedata.N_instances; n++) {
		cpuaabbsvec.push_back(cpuaabbs[n]);
	}
	// Sort the vector now
	sort(cpuaabbsvec.begin(), cpuaabbsvec.end());
	// Free the cpu array, not needed anymore
	free(cpuaabbs);
	// Copy the sorted data back to the GPU
	HANDLE_ERROR(cudaMemcpy(dev_cpuaabbs, cpuaabbsvec.data(), sizeof(cpuAABB) * scenedata.N_instances, cudaMemcpyHostToDevice));
	// Run kernel to reorder the bounding boxes according to the sort order
	ReorderAABBsOnGPU <<< (unsigned int)ceil(scenedata.N_instances / (float)256), 256 >>>(scenedata.bvhmem, dev_cpuaabbs, scenedata.N_instances);
	HANDLE_ERROR(cudaGetLastError());
	// Free on GPU when done
	HANDLE_ERROR(cudaThreadSynchronize());
	HANDLE_ERROR(cudaFree(dev_cpuaabbs));
}

// Helper function for binding all textures correctly
void SetupTextures(SceneMetaData scenedata) {
	//cudaChannelFormatDesc float2desc = cudaCreateChannelDesc<float2>();
	cudaChannelFormatDesc floatdesc = cudaCreateChannelDesc<float>();
	// Lights
	assert(sizeof(Light) % 4 == 0);
	if (scenedata.N_lights > 0) HANDLE_ERROR(cudaBindTexture(NULL, &LIGHTS_TEX, scenedata.lights, &floatdesc, scenedata.N_lights * sizeof(Light)));
	// Normals
	assert(sizeof(Normal) % 4 == 0);
	if (scenedata.N_normals > 0) HANDLE_ERROR(cudaBindTexture(NULL, &NORMALS_TEX, scenedata.normals, &floatdesc, scenedata.N_normals * sizeof(Normal)));
	// Texcoords
	assert(sizeof(Texcoord) % 4 == 0);
	if (scenedata.N_texcoords > 0) HANDLE_ERROR(cudaBindTexture(NULL, &TEXCOORDS_TEX, scenedata.texcoords, &floatdesc, scenedata.N_texcoords * sizeof(Texcoord)));
	// Vertices
	assert(sizeof(Vertex) % 4 == 0);
	if (scenedata.N_vertices > 0) HANDLE_ERROR(cudaBindTexture(NULL, &VERTICES_TEX, scenedata.vertices, &floatdesc, scenedata.N_vertices * sizeof(Vertex)));
	// Materials
	assert(sizeof(Material) % 4 == 0);
	if (scenedata.N_materials > 0) HANDLE_ERROR(cudaBindTexture(NULL, &MATERIALS_TEX, scenedata.materials, &floatdesc, scenedata.N_materials * sizeof(Material)));
	// Transforms
	assert(sizeof(Transform) % 4 == 0);
	if (scenedata.N_transforms > 0) HANDLE_ERROR(cudaBindTexture(NULL, &TRANSFORMS_TEX, scenedata.transforms, &floatdesc, scenedata.N_transforms * sizeof(Transform)));
	// Instances
	assert(sizeof(Instance) % 4 == 0);
	if (scenedata.N_instances > 0) HANDLE_ERROR(cudaBindTexture(NULL, &INSTANCES_TEX, scenedata.instances, &floatdesc, scenedata.N_instances * sizeof(Instance)));
	// Triangles
	assert(sizeof(Triangle) % 4 == 0);
	if (scenedata.N_triangles > 0) HANDLE_ERROR(cudaBindTexture(NULL, &TRIANGLES_TEX, scenedata.triangles, &floatdesc, scenedata.N_triangles * sizeof(Triangle)));
	// Spheres
	assert(sizeof(Sphere) % 4 == 0);
	if (scenedata.N_spheres > 0) HANDLE_ERROR(cudaBindTexture(NULL, &SPHERES_TEX, scenedata.spheres, &floatdesc, scenedata.N_spheres * sizeof(Sphere)));
	// Boxes
	assert(sizeof(Box) % 4 == 0);
	if (scenedata.N_boxes > 0) HANDLE_ERROR(cudaBindTexture(NULL, &BOXES_TEX, scenedata.boxes, &floatdesc, scenedata.N_boxes * sizeof(Box)));
	// Now the actual textures
	assert(sizeof(RGBQUAD) == sizeof(float));
	if (scenedata.N_images >= 1) {
		HANDLE_ERROR(cudaBindTexture2D(&image_texture_0_offset, &image_texture_0, scenedata.images[0].rgbquads, &floatdesc, scenedata.images[0].width, scenedata.images[0].height, scenedata.images[0].pitch));
		image_texture_0.filterMode = cudaFilterModePoint;
		image_texture_0.addressMode[0] = cudaAddressModeWrap;
		image_texture_0.addressMode[1] = cudaAddressModeWrap;
		image_texture_0.normalized = true;
	}
	if (scenedata.N_images >= 2) {
		HANDLE_ERROR(cudaBindTexture2D(&image_texture_1_offset, &image_texture_1, scenedata.images[1].rgbquads, &floatdesc, scenedata.images[1].width, scenedata.images[1].height, scenedata.images[1].pitch));
		image_texture_1.filterMode = cudaFilterModePoint;
		image_texture_1.addressMode[0] = cudaAddressModeWrap;
		image_texture_1.addressMode[1] = cudaAddressModeWrap;
		image_texture_1.normalized = true;
	}
	if (scenedata.N_images >= 3) {
		HANDLE_ERROR(cudaBindTexture2D(&image_texture_2_offset, &image_texture_2, scenedata.images[2].rgbquads, &floatdesc, scenedata.images[2].width, scenedata.images[2].height, scenedata.images[2].pitch));
		image_texture_2.filterMode = cudaFilterModePoint;
		image_texture_2.addressMode[0] = cudaAddressModeWrap;
		image_texture_2.addressMode[1] = cudaAddressModeWrap;
		image_texture_2.normalized = true;
	}
	if (scenedata.N_images >= 4) {
		HANDLE_ERROR(cudaBindTexture2D(&image_texture_3_offset, &image_texture_3, scenedata.images[3].rgbquads, &floatdesc, scenedata.images[3].width, scenedata.images[3].height, scenedata.images[3].pitch));
		image_texture_3.filterMode = cudaFilterModePoint;
		image_texture_3.addressMode[0] = cudaAddressModeWrap;
		image_texture_3.addressMode[1] = cudaAddressModeWrap;
		image_texture_3.normalized = true;
	}
	if (scenedata.N_images >= 5) {
		HANDLE_ERROR(cudaBindTexture2D(&image_texture_4_offset, &image_texture_4, scenedata.images[4].rgbquads, &floatdesc, scenedata.images[4].width, scenedata.images[4].height, scenedata.images[4].pitch));
		image_texture_4.filterMode = cudaFilterModePoint;
		image_texture_4.addressMode[0] = cudaAddressModeWrap;
		image_texture_4.addressMode[1] = cudaAddressModeWrap;
		image_texture_4.normalized = true;
	}
	if (scenedata.N_images >= 6) {
		HANDLE_ERROR(cudaBindTexture2D(&image_texture_5_offset, &image_texture_5, scenedata.images[5].rgbquads, &floatdesc, scenedata.images[5].width, scenedata.images[5].height, scenedata.images[5].pitch));
		image_texture_5.filterMode = cudaFilterModePoint;
		image_texture_5.addressMode[0] = cudaAddressModeWrap;
		image_texture_5.addressMode[0] = cudaAddressModeWrap;
		image_texture_5.normalized = true;
	}
}

// Helper function for unbinding all textures
void UnbindTextures() {
	// Lights
	HANDLE_ERROR(cudaUnbindTexture(&LIGHTS_TEX));
	// Normals
	HANDLE_ERROR(cudaUnbindTexture(&NORMALS_TEX));
	// Texcoords
	HANDLE_ERROR(cudaUnbindTexture(&TEXCOORDS_TEX));
	// Vertices
	HANDLE_ERROR(cudaUnbindTexture(&VERTICES_TEX));
	// Materials
	HANDLE_ERROR(cudaUnbindTexture(&MATERIALS_TEX));
	// Transforms
	HANDLE_ERROR(cudaUnbindTexture(&TRANSFORMS_TEX));
	// Instances
	HANDLE_ERROR(cudaUnbindTexture(&INSTANCES_TEX));
	// Triangles
	HANDLE_ERROR(cudaUnbindTexture(&TRIANGLES_TEX));
	// Spheres
	HANDLE_ERROR(cudaUnbindTexture(&SPHERES_TEX));
	// Boxes
	HANDLE_ERROR(cudaUnbindTexture(&BOXES_TEX));
	// BVH
	HANDLE_ERROR(cudaUnbindTexture(&BVH_TEX));
	// Textures
	HANDLE_ERROR(cudaUnbindTexture(&image_texture_0));
	HANDLE_ERROR(cudaUnbindTexture(&image_texture_1));
	HANDLE_ERROR(cudaUnbindTexture(&image_texture_2));
	HANDLE_ERROR(cudaUnbindTexture(&image_texture_3));
	HANDLE_ERROR(cudaUnbindTexture(&image_texture_4));
	HANDLE_ERROR(cudaUnbindTexture(&image_texture_5));
}

// These are inline getters to properly get the data from the textures - NO BOUNDS CHECKING... errors will happen if you go out of bounds
__device__  Light getLight(unsigned int index) {
	// Get the data
	Light light;
	for (int f = 0; f < sizeof(Light) / 4; f++) {
		((float*)&light)[f] = tex1Dfetch(LIGHTS_TEX, index * (sizeof(Light)/4) + f);
	}
	return light;
}
__device__  Normal getNormal(unsigned int index) {
	// Get the data
	Normal normal;
	for (int f = 0; f < sizeof(Normal) / 4; f++) {
		((float*)&normal)[f] = tex1Dfetch(NORMALS_TEX, index * (sizeof(Normal) / 4) + f);
	}
	return normal;
}
__device__  Texcoord getTexcoord(unsigned int index) {
	// Get the data
	Texcoord texcoord;
	for (int f = 0; f < sizeof(Texcoord) / 4; f++) {
		((float*)&texcoord)[f] = tex1Dfetch(TEXCOORDS_TEX, index * (sizeof(Texcoord) / 4) + f);
	}
	return texcoord;
}
__device__  Vertex getVertex(unsigned int index) {
	// Get the data
	Vertex vertex;
	for (int f = 0; f < sizeof(Vertex) / 4; f++) {
		((float*)&vertex)[f] = tex1Dfetch(VERTICES_TEX, index * (sizeof(Vertex) / 4) + f);
	}
	return vertex;
}
__device__  Material getMaterial(unsigned int index) {
	// Get the data
	Material material;
	for (int f = 0; f < sizeof(Material) / 4; f++) {
		((float*)&material)[f] = tex1Dfetch(MATERIALS_TEX, index * (sizeof(Material) / 4) + f);
	}
	return material;
}
__device__  Transform getTransform(unsigned int index) {
	// Get the data
	Transform transform;
	for (int f = 0; f < sizeof(Transform) / 4; f++) {
		((float*)&transform)[f] = tex1Dfetch(TRANSFORMS_TEX, index * (sizeof(Transform) / 4) + f);
	}
	return transform;
}
__device__  Instance getInstance(unsigned int index) {
	// Get the data
	Instance instance;
	for (int f = 0; f < sizeof(Instance) / 4; f++) {
		((float*)&instance)[f] = tex1Dfetch(INSTANCES_TEX, index * (sizeof(Instance) / 4) + f);
	}
	return instance;
}
__device__  Triangle getTriangle(unsigned int index) {
	// Get the data
	Triangle triangle;
	for (int f = 0; f < sizeof(Triangle) / 4; f++) {
		((float*)&triangle)[f] = tex1Dfetch(TRIANGLES_TEX, index * (sizeof(Triangle) / 4) + f);
	}
	return triangle;
}
__device__  Sphere getSphere(unsigned int index) {
	// Get the data
	Sphere sphere;
	for (int f = 0; f < sizeof(Sphere) / 4; f++) {
		((float*)&sphere)[f] = tex1Dfetch(SPHERES_TEX, index * (sizeof(Sphere) / 4) + f);
	}
	return sphere;
}
__device__  Box getBox(unsigned int index) {
	// Get the data
	Box box;
	for (int f = 0; f < sizeof(Box) / 4; f++) {
		((float*)&box)[f] = tex1Dfetch(BOXES_TEX, index * (sizeof(Box) / 4) + f);
	}
	return box;
}
__device__  AABBnode getAABBnode(unsigned int index) {
	// Get the data
	AABBnode aabb;
	for (int f = 0; f < sizeof(AABBnode) / 4; f++) {
		((float*)&aabb)[f] = tex1Dfetch(BVH_TEX, index * (sizeof(AABBnode) / 4) + f);
	}
	return aabb;
}

__device__ Vector3 imageTexFetch2D(unsigned int id, float x, float y) {
	float f;
	switch (id) {
	case 0:
		f = tex2D(image_texture_0, x, y);
		break;
	case 1:
		f = tex2D(image_texture_1, x, y);
		break;
	case 2:
		f = tex2D(image_texture_2, x, y);
		break;
	case 3:
		f = tex2D(image_texture_3, x, y);
		break;
	case 4:
		f = tex2D(image_texture_4, x, y);
		break;
	case 5:
		f = tex2D(image_texture_5, x, y);
		break;
	default:
		f = 0;
		break;
	}
	RGBQUAD* rgbquad = (RGBQUAD*)&f;
	return Vector3((float)(rgbquad->rgbRed) / (float)255, (float)(rgbquad->rgbGreen) / (float)255, (float)(rgbquad->rgbBlue) / (float)255);
}

#undef FOR_CUDA



