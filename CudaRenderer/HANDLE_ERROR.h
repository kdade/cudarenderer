// Helpful for cuda debugging
#ifndef CR_HANDLE_ERROR_H
#define CR_HANDLE_ERROR_H

#include <iostream>
#include <cuda_runtime.h>

using namespace std;

static void HandleError(cudaError_t err, const char *file, int line) {
	if (err != cudaSuccess) {
		printf("%s in %s at line %d\n", cudaGetErrorString(err), file, line);
		cout << "ENTER to exit" << endl;
		getchar();
		exit(EXIT_FAILURE);
	}
}

#define HANDLE_ERROR( err ) (HandleError( err, __FILE__, __LINE__ ))

#endif