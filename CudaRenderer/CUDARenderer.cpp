// Deals with all memory management between CPU/GPU

// Asser
#include <assert.h>

// OpenGL includes
#include <GLEW\glew.h>
#include <GLFW\glfw3.h>

// CUDA includes
#include <cuda_runtime.h>
#include <cuda_gl_interop.h>
#include <device_launch_parameters.h>

// Local includes
#include "HANDLE_ERROR.h"
#include "CUDARenderer.h"

extern "C" void runKernels(float4*, SceneMetaData);


CUDARenderer::CUDARenderer(int resx, int resy) {
	// First we get our cuda device ready
	HANDLE_ERROR(cudaDeviceSynchronize());
	HANDLE_ERROR(cudaDeviceReset());
	HANDLE_ERROR(cudaGLSetGLDevice(0));
	// Store input
	gpu_scene_data.resx = resx;
	gpu_scene_data.resy = resy;
	// Initialize view to identity
	gpu_scene_data.camera = Camera();
	gpu_scene_data.antialiasrate = 1;
	// Initialize BVH mode
	gpu_scene_data.bvhmode = BVHMODE_NONE;
	// Initialize render flags
	gpu_scene_data.ambient_on = true;
	gpu_scene_data.diffuse_on = true;
	gpu_scene_data.specular_on = true;
	gpu_scene_data.reflection_on = false;
	gpu_scene_data.transmission_on = false;
	gpu_scene_data.attenuation_on = false;
	// Clear color
	gpu_scene_data.clear_color = Vector3(0.0);
	// Ambient light to full if nothing else specified
	gpu_scene_data.ambient_light = Vector3(1.0);;
	// CPU Mesh vectors
	meshes = vector<Mesh>();
	meshInstances = vector<Instance>();
	// Initialize all vectors
	images = vector<CRImage>();
	lights = vector<Light>();
	normals = vector<Normal>();
	texcoords = vector<Texcoord>();
	vertices = vector<Vertex>();
	materials = vector<Material>();
	transforms = vector<Transform>();
	instances = vector<Instance>();
	triangles = vector<Triangle>();
	spheres = vector<Sphere>();
	boxes = vector<Box>();
	// No data on GPU yet
	dataAllocdOnGPU = false;
}

CUDARenderer::~CUDARenderer() {
	// Take care of PBO
	if (pbo) {
		glDeleteBuffers(1, &pbo);
		pbo = 0;
	}
	// Take care of Texture
	if (tex) {
		glDeleteTextures(1, &tex);
		tex = 0;
	}
	// Clean up pixel arrays
	// Free GPU data
	freeDataOnGPU();
}

// This registers all the necessary OpenGL stuff with CUDA
void CUDARenderer::initGLStructures() {
	// Create PBO - we will be writing into this...
	glGenBuffers(1, &pbo);
	glBindBuffer(GL_PIXEL_PACK_BUFFER, pbo);
	glBufferData(GL_PIXEL_PACK_BUFFER, gpu_scene_data.resx*gpu_scene_data.resy*sizeof(float) * 4, NULL, GL_DYNAMIC_DRAW);
	// Register the PBO with CUDA
	HANDLE_ERROR(cudaGLRegisterBufferObject(pbo));
	// Create Texture - PBO will be converted to this and then rendered, all on GPU
	glGenTextures(1, &tex);
	glBindTexture(GL_TEXTURE_2D, tex);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, gpu_scene_data.resx, gpu_scene_data.resy, 0, GL_RGBA, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
}

// This converts all object meshes into the necessary vertices, triangles, instances etc...
void CUDARenderer::convertObjectsToPrimitives() {
	for (size_t i = 0; i < meshes.size(); i++) {
		Mesh mesh = meshes[i];
		// All of the data for this mesh needs to go into the main vectors
		unsigned int firstVertex = N_vertices;
		for (Vertex v : mesh.vertices) {
			addVertex(v);
		}
		unsigned int firstNormal = N_normals;
		for (Normal n : mesh.normals) {
			normals.push_back(n);
			N_normals++;
		}
		unsigned int firstTexcoord = N_texcoords;
		for (Texcoord t : mesh.texcoords) {
			texcoords.push_back(t);
			N_texcoords++;
		}
		// Figure out which instances need to be added
		vector<Instance> myinstances = vector<Instance>();
		for (Instance instance : meshInstances) {
			if (instance.id == i) {
				myinstances.push_back(instance);
			}
		}

		// Add the triangles, indices must be adjusted...
		for (Triangle t : mesh.triangles) {
			t.vert_id[0] += firstVertex;
			t.vert_id[1] += firstVertex;
			t.vert_id[2] += firstVertex;
			t.normal_id[0] += firstNormal;
			t.normal_id[1] += firstNormal;
			t.normal_id[2] += firstNormal;
			t.texcoord_id[0] += firstTexcoord;
			t.texcoord_id[1] += firstTexcoord;
			t.texcoord_id[2] += firstTexcoord;
			unsigned int tri_id = addTriangle(t);
			// Add as many instances as required
			for (Instance instance : myinstances) {
				instance.type = TRIANGLE;
				instance.id = tri_id;
				addInstance(instance);
			}
		}
	}
}

// This pushes all of the data to the GPU
void CUDARenderer::pushToGPU() {
	// Now we need to start allocating global memory
	// The images to be used for textures:
	gpu_scene_data.N_images = N_images;
	for (unsigned int n = 0; n < N_images; n++) {
		CRImage gpu_image;
		gpu_image.width = images[n].width;
		gpu_image.height = images[n].height;
		HANDLE_ERROR(cudaMallocPitch((void**)&gpu_image.rgbquads, &gpu_image.pitch, gpu_image.width * sizeof(RGBQUAD), gpu_image.height));
		HANDLE_ERROR(cudaMemcpy2D(gpu_image.rgbquads, gpu_image.pitch, images[n].rgbquads, images[n].pitch, gpu_image.width * sizeof(RGBQUAD), gpu_image.height, cudaMemcpyHostToDevice));
		gpu_scene_data.images[n] = gpu_image;
	}
	// Light
	gpu_scene_data.N_lights = N_lights;
	HANDLE_ERROR(cudaMalloc((void**)&gpu_scene_data.lights, sizeof(Light) * N_lights));
	HANDLE_ERROR(cudaMemcpy(gpu_scene_data.lights, lights.data(), sizeof(Light) * N_lights, cudaMemcpyHostToDevice));
	// Normal
	gpu_scene_data.N_normals = N_normals;
	HANDLE_ERROR(cudaMalloc((void**)&gpu_scene_data.normals, sizeof(Normal) * N_normals));
	HANDLE_ERROR(cudaMemcpy(gpu_scene_data.normals, normals.data(), sizeof(Normal) * N_normals, cudaMemcpyHostToDevice));
	// Texcoord
	gpu_scene_data.N_texcoords = N_texcoords;
	HANDLE_ERROR(cudaMalloc((void**)&gpu_scene_data.texcoords, sizeof(Texcoord) * N_texcoords));
	HANDLE_ERROR(cudaMemcpy(gpu_scene_data.texcoords, texcoords.data(), sizeof(Texcoord) * N_texcoords, cudaMemcpyHostToDevice));
	// Vertex
	gpu_scene_data.N_vertices = N_vertices;
	HANDLE_ERROR(cudaMalloc((void**)&gpu_scene_data.vertices, sizeof(Vertex) * N_vertices));
	HANDLE_ERROR(cudaMemcpy(gpu_scene_data.vertices, vertices.data(), sizeof(Vertex) * N_vertices, cudaMemcpyHostToDevice));
	// Material
	gpu_scene_data.N_materials = N_materials;
	HANDLE_ERROR(cudaMalloc((void**)&gpu_scene_data.materials, sizeof(Material) * N_materials));
	HANDLE_ERROR(cudaMemcpy(gpu_scene_data.materials, materials.data(), sizeof(Material) * N_materials, cudaMemcpyHostToDevice));
	// Transform
	gpu_scene_data.N_transforms = N_transforms;
	HANDLE_ERROR(cudaMalloc((void**)&gpu_scene_data.transforms, sizeof(Transform) * N_transforms));
	HANDLE_ERROR(cudaMemcpy(gpu_scene_data.transforms, transforms.data(), sizeof(Transform) * N_transforms, cudaMemcpyHostToDevice));
	// Instance
	gpu_scene_data.N_instances = N_instances;
	HANDLE_ERROR(cudaMalloc((void**)&gpu_scene_data.instances, sizeof(Instance) * N_instances));
	HANDLE_ERROR(cudaMemcpy(gpu_scene_data.instances, instances.data(), sizeof(Instance) * N_instances, cudaMemcpyHostToDevice));
	// Triangles
	gpu_scene_data.N_triangles = N_triangles;
	HANDLE_ERROR(cudaMalloc((void**)&gpu_scene_data.triangles, sizeof(Triangle) * N_triangles));
	HANDLE_ERROR(cudaMemcpy(gpu_scene_data.triangles, triangles.data(), sizeof(Triangle) * N_triangles, cudaMemcpyHostToDevice));
	// Sphere
	gpu_scene_data.N_spheres = N_spheres;
	HANDLE_ERROR(cudaMalloc((void**)&gpu_scene_data.spheres, sizeof(Sphere) * N_spheres));
	HANDLE_ERROR(cudaMemcpy(gpu_scene_data.spheres, spheres.data(), sizeof(Sphere) * N_spheres, cudaMemcpyHostToDevice));
	// Box
	gpu_scene_data.N_boxes = N_boxes;
	HANDLE_ERROR(cudaMalloc((void**)&gpu_scene_data.boxes, sizeof(Box) * N_boxes));
	HANDLE_ERROR(cudaMemcpy(gpu_scene_data.boxes, boxes.data(), sizeof(Box) * N_boxes, cudaMemcpyHostToDevice));

	// Acceleration structures
	buildAccelerationStructures();
	// To help decide whether or not to free!
	dataAllocdOnGPU = true;
}

void CUDARenderer::freeDataOnGPU() {
	if (dataAllocdOnGPU) {
		// Pixel arrays
		CRImage cpuimage = CRImage();
		for (unsigned int n = 0; n < gpu_scene_data.N_images; n++) {
			//HANDLE_ERROR(cudaMemcpy(&cpuimage, &gpu_scene_data.images[n], sizeof(CRImage), cudaMemcpyDeviceToHost));
			HANDLE_ERROR(cudaFree(gpu_scene_data.images[n].rgbquads));
		}
		gpu_scene_data.N_images = 0;
		// Individual arrays
		HANDLE_ERROR(cudaFree(gpu_scene_data.lights));		gpu_scene_data.N_lights = 0;
		HANDLE_ERROR(cudaFree(gpu_scene_data.vertices));	gpu_scene_data.N_vertices = 0;
		HANDLE_ERROR(cudaFree(gpu_scene_data.texcoords));	gpu_scene_data.N_texcoords = 0;
		HANDLE_ERROR(cudaFree(gpu_scene_data.normals));		gpu_scene_data.N_normals = 0;
		HANDLE_ERROR(cudaFree(gpu_scene_data.materials));	gpu_scene_data.N_materials = 0;
		HANDLE_ERROR(cudaFree(gpu_scene_data.transforms));	gpu_scene_data.N_transforms = 0;
		HANDLE_ERROR(cudaFree(gpu_scene_data.instances));	gpu_scene_data.N_instances = 0;
		HANDLE_ERROR(cudaFree(gpu_scene_data.triangles));	gpu_scene_data.N_triangles = 0;
		HANDLE_ERROR(cudaFree(gpu_scene_data.spheres));		gpu_scene_data.N_spheres = 0;
		HANDLE_ERROR(cudaFree(gpu_scene_data.boxes));		gpu_scene_data.N_boxes = 0;
		// Accel
		HANDLE_ERROR(cudaFree(gpu_scene_data.bvhmem));
		dataAllocdOnGPU = false;
	}
}

// This tells the GPU to go ahead and build the accel. structures - IMPORTANT: Assumes that all instances are already loaded
void CUDARenderer::buildAccelerationStructures() {
	if (gpu_scene_data.N_instances == 0)
		return;

	unsigned int N_nodes = 0;
	unsigned int N_leaves = 0;
	unsigned int N_bvhmaxdepth = 0;
	unsigned int height;
	if (gpu_scene_data.bvhmode == BVHMODE_TREE) {
		// Basic spatial partition tree, fixed resolution ----------------------
		height = (unsigned int)ceil(log2(N_instances));
		N_leaves = (unsigned int)pow(2, height);
		// Calculate number of nodes needed
		unsigned int N = N_instances;
		for (unsigned int n = 0; n <= height; n++) {
			N_nodes += N;
			N = (N + 1) / 2;
		}
		N_nodes = (N_nodes < 2 * N_instances) ? 2 * N_instances : N_nodes;
		N_bvhmaxdepth = height + 1;
		gpu_scene_data.useBVH = true;
		cout << "Building acceleration structure: Binary tree with height " << height << "..." << endl;
	}
	else {
		// Fall-through: Only BVH is the bounding box for the scene - done on CPU, no reason to offload to GPU
		N_nodes = gpu_scene_data.N_instances + 1;
		N_leaves = N_instances;
		N_bvhmaxdepth = N_nodes;
		gpu_scene_data.useBVH = false;
		cout << "Building bounding volume for scene..." << endl;
	}
	HANDLE_ERROR(cudaMalloc((void**)&gpu_scene_data.bvhmem, sizeof(AABBnode) * N_nodes));
	gpu_scene_data.N_bvhnodes = N_nodes;
	gpu_scene_data.N_bvhleaves = N_leaves;
	gpu_scene_data.N_bvhmaxdepth = N_bvhmaxdepth;
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//
//								THESE ARE VISIBLE TO THE OUTSIDE WORLD
//
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Send refresh what is drawn in the window
void CUDARenderer::refresh(int wwidth, int wheight) {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Use the pixels in the PBO to generate the Texture
	glBindBuffer(GL_PIXEL_UNPACK_BUFFER, pbo);
	glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, gpu_scene_data.resx, gpu_scene_data.resy, GL_RGBA, GL_FLOAT, NULL);
	glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0);

	// Draw a fullscreen quad with the texture.
	glViewport(0, 0, wwidth, wheight);
	glBegin(GL_QUADS);
	glTexCoord2f(0.0f, 1.0f); glVertex3f(0.0f, 0.0f, 0.0f);
	glTexCoord2f(0.0f, 0.0f); glVertex3f(0.0f, 1.0f, 0.0f);
	glTexCoord2f(1.0f, 0.0f); glVertex3f(1.0f, 1.0f, 0.0f);
	glTexCoord2f(1.0f, 1.0f); glVertex3f(1.0f, 0.0f, 0.0f);
	glEnd();
}

// Render the image to the pixel buffer
void CUDARenderer::render() {
	// Convert all objects to primitives
	convertObjectsToPrimitives();

	// Quick check...
	if (N_instances == 0) {
		// nothing to render...
		cout << "Nothing to render, showing most recent render image..." << endl;
		return;
	}

	// Map the PBO onto the device
	float4* pixels = NULL;
	HANDLE_ERROR(cudaGLMapBufferObject((void**)&pixels, pbo));

	// Make sure to keep everything current
	pushToGPU();

	// Run kernel to generate the pixels
	runKernels(pixels, gpu_scene_data);

	// Finish all threads and unmap pbo
	HANDLE_ERROR(cudaDeviceSynchronize());
	HANDLE_ERROR(cudaGLUnmapBufferObject(pbo));

	// Cleanup on GPU
	freeDataOnGPU();
}

// Set the BVHmode
void CUDARenderer::setBVHMode(BVHMODE mode, int param) {
	gpu_scene_data.bvhmode = mode;
	gpu_scene_data.bvhparam = param;
}

// Clear all the scene data
void CUDARenderer::clearScene() {
	gpu_scene_data.camera = Camera();
	gpu_scene_data.antialiasrate = 1;
	// mesh data
	meshes.clear();
	meshInstances.clear();
	// Image need to be unloaded
	for (CRImage image : images) image.Cleanup();
	// cpu data
	images.clear();			N_images = 0;
	lights.clear();			N_lights = 0;
	normals.clear();		N_normals = 0;
	texcoords.clear();		N_texcoords = 0;
	vertices.clear();		N_vertices = 0;
	materials.clear();		N_materials = 0;
	transforms.clear();		N_transforms = 0;
	instances.clear();		N_instances = 0;
	triangles.clear();		N_triangles = 0;
	spheres.clear();		N_spheres = 0;
	boxes.clear();			N_boxes = 0;
	freeDataOnGPU();
}

// Adjust the pixel resolution
void CUDARenderer::setResolution(int resx, int resy) {
	gpu_scene_data.resx = resx;
	gpu_scene_data.resy = resy;
	// Reformat the pixel buffer
	HANDLE_ERROR(cudaGLUnregisterBufferObject(pbo));
	glBindBuffer(GL_PIXEL_PACK_BUFFER, pbo);
	glBufferData(GL_PIXEL_PACK_BUFFER, resx*resy*sizeof(float) * 4, NULL, GL_DYNAMIC_DRAW);
	HANDLE_ERROR(cudaGLRegisterBufferObject(pbo));
	// And the texture
	glBindTexture(GL_TEXTURE_2D, tex);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, resx, resy, 0, GL_RGBA, GL_FLOAT, NULL);
}

// Sets the camera
void CUDARenderer::setCamera(Camera c) {
	gpu_scene_data.camera = c;
}

// Set the anti-aliasing rate
void CUDARenderer::setAntiAlias(unsigned int rate) {
	if (rate == 0) rate = 1;
	gpu_scene_data.antialiasrate = rate;
}

// Adds a transform
int CUDARenderer::addTransform(Transform t) {
	transforms.push_back(t);
	return N_transforms++;
}

// Set the ambient light
int CUDARenderer::setAmbientLight(Vector3 rgb) {
	gpu_scene_data.ambient_light = rgb;
	return 0;
}

// Load an image for use as a texture.
int CUDARenderer::addImage(const char* filename) {
	if (N_images >= MAX_TEXTURES) {
		cout << "ERROR: Image memory is full. CUDARenderer supports up to " << MAX_TEXTURES << " image textures at a time." << endl;
		return -1;
	}
	images.push_back(CRImage(filename));
	return N_images++;
}

// Adds a wavefront .obj specified object mesh to the scene
int CUDARenderer::addObject(const char* filename) {
	vector<tinyobj::material_t> materials = vector<tinyobj::material_t>();
	vector<tinyobj::shape_t> shapes = vector<tinyobj::shape_t>();
	tinyobj::LoadObj(shapes, materials, filename);
	// Now do some finagling...
	Mesh mesh = Mesh();
	for (unsigned int i = 0; i < shapes.size(); i++) {
		// Vertices
		assert((shapes[i].mesh.positions.size() % 3) == 0);
		for (size_t v = 0; v < shapes[i].mesh.positions.size() / 3; v++) {
			Vertex vertex;
			vertex.pos = Vector3(shapes[i].mesh.positions[3 * v + 0], shapes[i].mesh.positions[3 * v + 1], shapes[i].mesh.positions[3 * v + 2]);
			mesh.vertices.push_back(vertex);
		}
		// Texcoords
		assert((shapes[i].mesh.texcoords.size() % 2) == 0);
		for (size_t f = 0; f < shapes[i].mesh.texcoords.size() / 2; f++) {
			Texcoord texcoord;
			texcoord.u = shapes[i].mesh.texcoords[2 * f + 0];
			texcoord.v = shapes[i].mesh.texcoords[2 * f + 1];
			mesh.texcoords.push_back(texcoord);
		}
		// Normals
		assert((shapes[i].mesh.normals.size() % 3) == 0);
		for (size_t v = 0; v < shapes[i].mesh.normals.size() / 3; v++) {
			Normal normal;
			normal.dir = host_normalize(Vector3(shapes[i].mesh.normals[3 * v + 0], shapes[i].mesh.normals[3 * v + 1], shapes[i].mesh.normals[3 * v + 2]));
			mesh.normals.push_back(normal);
		}
		// Triangles
		assert((shapes[i].mesh.indices.size() % 3) == 0);
		for (size_t f = 0; f < shapes[i].mesh.indices.size() / 3; f++) {
			Triangle t;
			// Initialize flags
			t.f.use_normals = shapes[i].mesh.normals.size() > 0 ? 1 : 0;
			t.f.use_texcoords = shapes[i].mesh.texcoords.size() > 0 ? 1 : 0;
			// Load indices
			t.vert_id[0] = shapes[i].mesh.indices[3 * f + 0];
			t.vert_id[1] = shapes[i].mesh.indices[3 * f + 1];
			t.vert_id[2] = shapes[i].mesh.indices[3 * f + 2];
			t.normal_id[0] = shapes[i].mesh.indices[3 * f + 0];
			t.normal_id[1] = shapes[i].mesh.indices[3 * f + 1];
			t.normal_id[2] = shapes[i].mesh.indices[3 * f + 2];
			t.texcoord_id[0] = shapes[i].mesh.indices[3 * f + 0];
			t.texcoord_id[1] = shapes[i].mesh.indices[3 * f + 1];
			mesh.triangles.push_back(t);
		}
	}
	// Done.
	meshes.push_back(mesh);
	// Debug
//cout << "Added mesh: " << meshes.size() - 1 << endl;
//cout << "\t # verts: " << mesh.vertices.size() << endl;
//cout << "\t # tris:  " << mesh.triangles.size() << endl;
//cout << "\t # normals: " << mesh.normals.size() << endl;
return meshes.size() - 1;
}

// Add a light
int CUDARenderer::addLight(Light l) {
	lights.push_back(l);
	return N_lights++;
}

// Adds a vertex and return the vertex id
int CUDARenderer::addVertex(Vertex v) {
	vertices.push_back(v);
	return N_vertices++;
}

// Adds a material and returns the material id (location in vector)
int CUDARenderer::addMaterial(Material m) {
	materials.push_back(m);
	return N_materials++;
}

// Adds a sphere
int CUDARenderer::addSphere(Sphere s) {
	spheres.push_back(s);
	return N_spheres++;
}

// Adds a triangle
int CUDARenderer::addTriangle(Triangle t) {
	// Check that vertices exist
	if (t.vert_id[0] >= N_vertices || t.vert_id[1] >= N_vertices || t.vert_id[2] >= N_vertices) {
		cout << "ERROR: cannot add triangle, vertex does not exist" << endl;
		return -1;
	}
	// Precompute the two sides
	Vector3 p1 = vertices[t.vert_id[0]].pos;
	Vector3 p2 = vertices[t.vert_id[1]].pos;
	Vector3 p3 = vertices[t.vert_id[2]].pos;
	t.e1 = p2 - p1;
	t.e2 = p3 - p1;
	triangles.push_back(t);
	return N_triangles++;
}

// Adds a box
int CUDARenderer::addBox(Box b) {
	// Check that vertices exist
	for (int n = 0; n < 8; n++)
		if (b.vert_id[0] > N_vertices) {
		cout << "ERROR: cannot add box, vertex does not exist" << endl;
		return -1;
		}
	boxes.push_back(b);
	return N_boxes++;
}

// Adds an instance
int CUDARenderer::addInstance(Instance i) {
	lastInstanceWasMesh = false;
	// Check that type exists...
	switch (i.type) {
	case TRIANGLE:
		if (i.id >= N_triangles) {
			cout << "ERROR: cannot add instance, triangle does not exist" << endl;
			return -1;
		}
		break;
	case SPHERE:
		if (i.id >= N_spheres) {
			cout << "ERROR: cannot add instance, sphere does not exist" << endl;
			return -1;
		}
		break;
	case BOX:
		if (i.id >= N_boxes) {
			cout << "ERROR: cannot add instance, box does not exist" << endl;
			return -1;
		}
		break;
	case OBJECT:
		if (i.id >= meshes.size()) {
			cout << "ERROR: cannot add instance, object does not exist" << endl;
			return -1;
		}
	}
	// Check transform exists
	if (i.tform_id >= N_transforms) {
		cout << "ERROR: cannot add instance, transform does not exist" << endl;
		return -1;
	}
	// Check material exists
	if (i.mat_id >= N_materials) {
		cout << "ERROR: cannot add instance, material does not exist" << endl;
		return -1;
	}
	// Check texture ids
	if (i.useColorTex && i.color_tex_id >= MAX_TEXTURES || i.useNormalTex && i.normal_tex_id >= MAX_TEXTURES) {
		return -1;
	}
	// If it is a mesh, stop here
	if (i.type == OBJECT) {
		meshInstances.push_back(i);
		lastInstanceWasMesh = true;
		return meshInstances.size() - 1;
	}
	// Precompute centroid
	Vector3 centroid;
	switch (i.type) {
	case TRIANGLE:
		centroid.x = (vertices[triangles[i.id].vert_id[0]].pos.x + vertices[triangles[i.id].vert_id[1]].pos.x + vertices[triangles[i.id].vert_id[2]].pos.x) / 3.0f;
		centroid.y = (vertices[triangles[i.id].vert_id[0]].pos.y + vertices[triangles[i.id].vert_id[1]].pos.y + vertices[triangles[i.id].vert_id[2]].pos.y) / 3.0f;
		centroid.z = (vertices[triangles[i.id].vert_id[0]].pos.z + vertices[triangles[i.id].vert_id[1]].pos.z + vertices[triangles[i.id].vert_id[2]].pos.z) / 3.0f;
		break;
	case SPHERE:
		centroid = spheres[i.id].pos;
		break;
	case BOX:
		for (int v = 0; v < 8; v++) {
			centroid.x += vertices[boxes[i.id].vert_id[v]].pos.x;
			centroid.y += vertices[boxes[i.id].vert_id[v]].pos.y;
			centroid.z += vertices[boxes[i.id].vert_id[v]].pos.z;
		}
		centroid /= 8.0f;
		break;
	default:
		break;
	}
	// Don't forget to transform it properly!!!
	i.centroid = transformPoint(transforms[i.tform_id], centroid);
	instances.push_back(i);
	return N_instances++;
}

// Remove an instance
void CUDARenderer::undoInstance() {
	if (lastInstanceWasMesh) {
		if (N_instances > 0) {
			instances.pop_back();
			N_instances--;
		}
	}
	else {
		if (meshInstances.size() > 0) {
			meshInstances.pop_back();
		}
	}
}

// This just spits out everything
void CUDARenderer::dump() {
	cout << "--------\n" << N_lights << " LIGHTS " << endl;
	//for (Light l : lights) cout << "<" << l.col.x << ", " << l.col.y << ", " << l.col.z << ">" << endl;
	cout << "--------\n" << N_vertices << " VERTICES " << endl;
	//for (Vertex v : vertices) cout << "<" << v.pos.x << ", " << v.pos.y << ", " << v.pos.z << ">" << endl;
	cout << "--------\n" << N_materials << " MATERIALS " << endl;
	cout << "--------\n" << N_transforms << " TRANSFORMS " << endl;
	cout << "--------\n" << N_triangles << " TRIANGLES " << endl;
	//for (Triangle t : triangles) cout << "(" << t.vert_id[0] << ", " << t.vert_id[1] << ", " << t.vert_id[2] << ")" << endl;
	cout << "--------\n" << N_spheres << " SPHERES " << endl;
	//for (Sphere s : spheres) cout << "<" << s.pos.x << ", " << s.pos.y << ", " << s.pos.z << ">" << endl;
	cout << "--------\n" << N_boxes << " BOXES " << endl;
	/*for (Box b : boxes) {
		cout << "(";
		for (int n = 0; n < 8; n++) cout << b.vert_id[n] << ", ";
		cout << ")" << endl;
	}*/
	cout << "--------\n" << N_instances << " INSTANCES " << endl;
	//for (Instance i : instances) cout << "type:	" << i.type << "   id: " << i.id << " <" << i.centroid.x << ", " << i.centroid.y << ", " << i.centroid.z << ">" << endl;
	cout << "--------" << endl;
}