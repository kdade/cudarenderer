#ifndef CR_CRIMAGE_H
#define CR_CRIMAGE_H

#include <FreeImage/FreeImage.h>
#include <cuda_runtime.h>
#include <iostream>

struct CRImage {
	unsigned int width;
	unsigned int height;
	unsigned int pitch;
	RGBQUAD* rgbquads;

	// Methods
	CRImage::CRImage();
	CRImage::CRImage(const char* imfile);
	void CRImage::Cleanup();
};

#endif