// Main entry point

// General use includes
#include <iostream>
#include <vector>

// OpenGL includes
#include <GLEW/glew.h>
#include <GLFW/glfw3.h>

// CUDA includes
#include <cuda_runtime.h>
#include <cuda_gl_interop.h>

// Lua
#include <Lua\lua.hpp>

// Local includes
#include "HANDLE_ERROR.h"
#include "GLFWBackend.h"
#include "CUDARenderer.h"

// Dimensions of rendered image
#define WIDTH 640
#define HEIGHT 480

using namespace std;

// Need to be public so that Lua can use them
static CUDARenderer* renderer;
static GLFWwindow* window;

// Useful for Lua
#define LUAFUN(tag) static int tag(lua_State* L)
// Lua functions
LUAFUN(lRender);
LUAFUN(lClear);
LUAFUN(lCamera);
LUAFUN(lResolution);
LUAFUN(lAntiAlias);
LUAFUN(lBVHMode);
LUAFUN(lTransform);
LUAFUN(lImage);
LUAFUN(lAmbientLight);
LUAFUN(lLight);
LUAFUN(lObject);
LUAFUN(lVertex);
LUAFUN(lMaterial);
LUAFUN(lTriangle);
LUAFUN(lSphere);
LUAFUN(lBox);
LUAFUN(lInstance);
LUAFUN(lUndoInstance);

// This registers all functions with the interpreter
void RegisterLuaFunctions(lua_State* L) {
	lua_register(L, "Render", lRender);
	lua_register(L, "Clear", lClear);
	lua_register(L, "Camera", lCamera);
	lua_register(L, "Resolution", lResolution);
	lua_register(L, "AntiAlias", lAntiAlias);
	lua_register(L, "BVHMode", lBVHMode);
	lua_register(L, "Texture", lImage);
	lua_register(L, "Transform", lTransform);
	lua_register(L, "AmbientLight", lAmbientLight);
	lua_register(L, "Light", lLight);
	lua_register(L, "Object", lObject);
	lua_register(L, "Vertex", lVertex);
	lua_register(L, "Material", lMaterial);
	lua_register(L, "Triangle", lTriangle);
	lua_register(L, "Sphere", lSphere);
	lua_register(L, "Box", lBox);
	lua_register(L, "Instance", lInstance);
	lua_register(L, "UndoInstance", lUndoInstance);
}


// Entry point
int main() {
	// ------------- INITIALIZATION ------------ //
	// Initialize CUDA first
	renderer = new CUDARenderer(WIDTH, HEIGHT);

	// Get GLFW and GLEW up and running
	window = createWindow(WIDTH, HEIGHT, "CS148 Cuda Renderer");

	// Set up Lua interpreter
	lua_State* L = lua_open();
	luaL_openlibs(L);
	// Open any custom code libs
	luaL_dofile(L, "../scripts/helpers.lua");
	// Register all c++ interface functions
	RegisterLuaFunctions(L);

	// Do OpenGL dependent setup for CUDA
	renderer->initGLStructures();

	// ------------ MAIN EVENT LOOP ------------ //
	cout << "Starting Lua interpreter..." << endl;
	char input[256];
	bool shouldEnd = false;
	while (!shouldEnd)  {
		cout << "   Lua: ";
		gets(input);
		if (strcmp(input, "exit") == 0) shouldEnd = true;
		else if (strcmp(input, "test") == 0) luaL_dofile(L, "../scripts/test.lua");
		else if (strcmp(input, "scene") == 0) luaL_dofile(L, "../scripts/scene.lua");
		else if (strcmp(input, "dump") == 0) renderer->dump();
		else luaL_dostring(L, input);
	} 

	// ------------ CLEANUP -------------- //
	// Cleanup all CUDA related stuff
	delete renderer;
	// Wrap up GLFW and GL context
	glfwDestroyWindow(window);
	glfwTerminate();
	// Close Lua
	lua_close(L);
	// Done
	return 0;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////// BELOW HERE IS PURELY FOR INTERFACE WITH LUA //////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// This renders the scene
LUAFUN(lRender) {
	double t1 = glfwGetTime();
	renderer->render();
	double t2 = glfwGetTime();
	cout << "Rendered in " << (t2 - t1) << "s" << endl;
	cout << "Close the render window to reactivate console input." << endl;
	// Keep the image current in the window
	glfwShowWindow(window);
	while (!glfwWindowShouldClose(window)) {
		int wwidth, wheight;
		glfwGetWindowSize(window, &wwidth, &wheight);
		renderer->refresh(wwidth, wheight);
		glfwSwapBuffers(window);
		glfwPollEvents();
	}
	glfwHideWindow(window);
	glfwSetWindowShouldClose(window, false);
	// No return values
	lua_pushinteger(L, 0);
	return 0;
}

// Clear the scene
LUAFUN(lClear) {
	renderer->clearScene();
	// No return values
	lua_pushinteger(L, 0);
	return 0;
}

// Set the camera
LUAFUN(lCamera) {
	if (lua_gettop(L) == CAMERA_N_ELEMS) {
		Camera camera;
		camera.eye = Vector3((float)lua_tonumber(L, 1), (float)lua_tonumber(L, 2), (float)lua_tonumber(L, 3));
		camera.up = Vector3((float)lua_tonumber(L, 4), (float)lua_tonumber(L, 5), (float)lua_tonumber(L, 6));
		camera.look = Vector3((float)lua_tonumber(L, 7), (float)lua_tonumber(L, 8), (float)lua_tonumber(L, 9));
		camera.FOV = (float)M_PI * (float)lua_tonumber(L, 10) / 180.f;
		camera.aperture = (float)lua_tonumber(L, 11);
		camera.eyeoffset = Vector3(0.0);
		renderer->setCamera(camera);
		lua_pushinteger(L, 0);
		return 0;
	}
	// Usage
	cout << "USAGE -- Camera(eye.x, eye.y, eye.z, up.x, up.y, up.z, look.x, look.y, look.z, FOVdegrees, aperture)" << endl;
	cout << "USAGE -- aperture >= 0 : affects the amount of depth of field centered around the look point" << endl;
	// No return values
	lua_pushinteger(L, 0);
	return -1;
}

// Set the resolution
LUAFUN(lResolution) {
	if (lua_gettop(L) == 2) {
		int w = lua_tointeger(L, 1);
		int h = lua_tointeger(L, 2);	
		cout << "Resolution set to " << w << " by " << h << endl;
		renderer->setResolution(w, h);
	}
	// No return values
	lua_pushinteger(L, 0);
	return 0;
}

// Set the anti-aliasing
LUAFUN(lAntiAlias) {
	if (lua_gettop(L) == 1) {
		renderer->setAntiAlias((unsigned int)lua_tointeger(L, 1));
		lua_pushinteger(L, 0);
		return 0;
	}
	// Usage
	cout << "USAGE -- AntiAlias(aaFactor) : aaFactor should be a positive integer." << endl;
	lua_pushinteger(L, 0);
	return -1;
}

// Set the mode for BVH
LUAFUN(lBVHMode) {
	if (lua_gettop(L) == 1) {
		char* modestr = (char*)lua_tostring(L, 1);
		if (strcmp(modestr, "none") == 0) renderer->setBVHMode(BVHMODE_NONE, 0);
		else if (strcmp(modestr, "tree") == 0) renderer->setBVHMode(BVHMODE_TREE, lua_tointeger(L, 2));
		// No return values
		lua_pushinteger(L, 0);
		return 0;
	}
	// Usage
	cout << "USAGE -- BVHMode(\"mode\")  :: modes can be \"none\", \"tree\"" << endl;
	// No return values
	lua_pushinteger(L, 0);
	return -1;
}

// Add an image for texturing
LUAFUN(lImage) {
	if (lua_gettop(L) == 1) {
		const char* filename = lua_tostring(L, 1);
		lua_pushinteger(L, renderer->addImage(filename));
		return 1;
	}
	cout << "USAGE -- Texture(\"imfile\")" << endl;
	return -1;
}

// Add a transform
LUAFUN(lTransform) {
	// Identity
	if (lua_gettop(L) == 0) {
		lua_pushinteger(L, renderer->addTransform(Transform()));
		return 1;
	}
	// User specified
	if (lua_gettop(L) == TRANSFORM_N_ELEMS) {
		float B[4][4];
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				B[i][j] = (float)lua_tonumber(L, i * 4 + j + 1);
			}
		}
		Transform t = Transform(B);
		lua_pushinteger(L, renderer->addTransform(t));
		return 1;
	}
	// Usage
	cout << "USAGE -- Transform() for identity, Transform (a11, a12, ... , a43, a44) for user specified" << endl;
	return -1;
}

// Set the ambient light
LUAFUN(lAmbientLight) {
	if (lua_gettop(L) == 3) {
		lua_pushinteger(L, renderer->setAmbientLight(Vector3((float)lua_tonumber(L, 1), (float)lua_tonumber(L, 2), (float)lua_tonumber(L, 3))));
		return 1;
	}
	// Usage
	cout << "USAGE -- AmbientLight(r, g, b)" << endl;
	return -1;
}

// Add a light
LUAFUN(lLight) {
	if (lua_gettop(L) == LIGHT_N_ELEMS) {
		Light l;
		l.pos = Vector3((float)lua_tonumber(L, 1), (float)lua_tonumber(L, 2), (float)lua_tonumber(L, 3));
		l.col = Vector3(Vector3((float)lua_tonumber(L, 4), (float)lua_tonumber(L, 5), (float)lua_tonumber(L, 6)));
		l.dir = host_normalize(Vector3(Vector3((float)lua_tonumber(L, 7), (float)lua_tonumber(L, 8), (float)lua_tonumber(L, 9))) - l.pos);
		l.cos_theta = (float)lua_tonumber(L, 10);
		lua_pushinteger(L, renderer->addLight(l));
		return 1;
	}
	// Usage
	cout << "USAGE -- Light(x, y, z, r, g, b, lookat_x, lookat_y, lookat_z, cos_theta) : Set cos_theta = -1 for point light" << endl;
	return -1;
}

// Add an object
LUAFUN(lObject) {
	if (lua_gettop(L) == 1) {
		const char* objfile = lua_tostring(L, 1);
		double t1 = glfwGetTime();
		lua_pushinteger(L, renderer->addObject(objfile));
		double t2 = glfwGetTime();
		cout << "Loaded object from file, \"" << objfile << "\", in " << t2 - t1 << " seconds." << endl;
		return 1;
	}
	// Usage
	cout << "USAGE -- Object(\"filename.obj\")" << endl;
	return -1;
}

// Add a vertex
LUAFUN(lVertex) {
	if (lua_gettop(L) == VERTEX_N_ELEMS) {
		Vertex v;
		v.pos = Vector3((float)lua_tonumber(L, 1), (float)lua_tonumber(L, 2), (float)lua_tonumber(L, 3));
		lua_pushinteger(L,  renderer->addVertex(v));
		return 1;
	}
	// Usage
	cout << "USAGE -- Vertex(x, y, z)" << endl;
	return -1;
}

// Add a material
LUAFUN(lMaterial) {
	if (lua_gettop(L) == MATERIAL_N_ELEMS) {
		Material m;
		m.ka = Vector3((float)lua_tonumber(L, 1), (float)lua_tonumber(L, 2), (float)lua_tonumber(L, 3));
		m.kd = Vector3((float)lua_tonumber(L, 4), (float)lua_tonumber(L, 5), (float)lua_tonumber(L, 6));
		m.ks = Vector3((float)lua_tonumber(L, 7), (float)lua_tonumber(L, 8), (float)lua_tonumber(L, 9));
		m.Ns = (float)lua_tonumber(L, 10);
		m.kr = Vector3((float)lua_tonumber(L, 11), (float)lua_tonumber(L, 12), (float)lua_tonumber(L, 13));
		m.n = (float)lua_tonumber(L, 14);
		m.c = Vector3((float)lua_tonumber(L, 15), (float)lua_tonumber(L, 16), (float)lua_tonumber(L, 17));
		lua_pushinteger(L, renderer->addMaterial(m));
		return 1;
	}
	// Usage
	cout << "USAGE -- Material(ka.r, ka.g, ka.b, kd.r, kd.g, kd.b, ks.r, ks.g, ks.b, Ns, kr.r, kr.g, kr.b, n, c.r, c.g, c.b)" << endl;
	return -1;
}

// Add a triangle
LUAFUN(lTriangle) {
	// The secret, abbreviated version...
	if (lua_gettop(L) == TRIANGLE_N_ELEMS) {
		Triangle t;
		t.vert_id[0] = (unsigned int)lua_tointeger(L, 1);
		t.vert_id[1] = (unsigned int)lua_tointeger(L, 2);
		t.vert_id[2] = (unsigned int)lua_tointeger(L, 3);
		t.f.use_normals = 0;
		t.f.use_texcoords = 0;
		lua_pushinteger(L, renderer->addTriangle(t));
		return 1;
	}
	// Usage
	cout << "USAGE -- Triangle(vert_id0, vert_id1, vert_id2)" << endl;
	return -1;
}

// Add a sphere
LUAFUN(lSphere) {
	if (lua_gettop(L) == SPHERE_N_ELEMS) {
		Sphere s;
		s.pos = Vector3((float)lua_tonumber(L, 1), (float)lua_tonumber(L, 2), (float)lua_tonumber(L, 3));
		s.r = (float)lua_tonumber(L, 4);
		lua_pushinteger(L, renderer->addSphere(s));
		return 1;
	}
	// Usage
	cout << "USAGE -- Sphere(x, y, z, r)" << endl;
	return -1;
}

// Add a box
LUAFUN(lBox) {
	if (lua_gettop(L) == BOX_N_ELEMS) {
		Box b;
		b.vert_id[0] = (unsigned int)lua_tointeger(L, 1);
		b.vert_id[1] = (unsigned int)lua_tointeger(L, 2);
		b.vert_id[2] = (unsigned int)lua_tointeger(L, 3);
		b.vert_id[3] = (unsigned int)lua_tointeger(L, 4);
		b.vert_id[4] = (unsigned int)lua_tointeger(L, 5);
		b.vert_id[5] = (unsigned int)lua_tointeger(L, 6);
		b.vert_id[6] = (unsigned int)lua_tointeger(L, 7);
		b.vert_id[7] = (unsigned int)lua_tointeger(L, 8);
		lua_pushinteger(L, renderer->addBox(b));
		return 1;
	}
	// Usage
	cout << "USAGE -- Box(vert_id0, vert_id1, ... vert_id7)" << endl;
	return -1;
}

// Add an instance
LUAFUN(lInstance) {
	if (lua_gettop(L) == INSTANCE_N_ELEMS) {
		Instance i;
		const char* typestr = lua_tostring(L, 1);
		unsigned int type = NAT;
		if (strcmp(typestr, "triangle") == 0) type = TRIANGLE;
		else if (strcmp(typestr, "sphere") == 0) type = SPHERE;
		else if (strcmp(typestr, "box") == 0) type = BOX;
		else if (strcmp(typestr, "object") == 0) type = OBJECT;
		if (type != NAT) {
			i.type = type;
			i.id = (unsigned int)lua_tointeger(L, 2);
			i.tform_id = (unsigned int)lua_tointeger(L, 3);
			i.mat_id = (unsigned int)lua_tointeger(L, 4);
			i.color_tex_id = (unsigned int)lua_tointeger(L, 5);
			i.useColorTex = (unsigned int)lua_toboolean(L, 6);
			i.normal_tex_id = (unsigned int)lua_tointeger(L, 7);
			i.useNormalTex = (unsigned int)lua_toboolean(L, 8);
			lua_pushinteger(L, renderer->addInstance(i));
			return 1;
		}
	}
	// Usage
	cout << "USAGE -- Instance(\"type\", id, tform_id, mat_id, color_tex_id, bool_useColorTex, normal_tex_id, bool_useNormalTex)" << endl;
	return -1;
}

// Remove the most recently added instance
LUAFUN(lUndoInstance) {
	if (lua_gettop(L) == 0) {
		renderer->undoInstance();
		// No return values
		lua_pushinteger(L, 0);
		return 0;
	}
	// Usage
	cout << "USAGE -- undoInstance() - removes the most recently added instance" << endl;
	// No return values
	lua_pushinteger(L, 0);
	return 0;
}
