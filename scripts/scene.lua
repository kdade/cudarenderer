-- clear everything
Clear()
-- set the resolution
Resolution(512, 512)
AntiAlias(9)
-- set the view point
--Camera(-4,2.5,0,0,1,0,0,0,-5,26,0.0)
--Camera(-1.5,3,-2,0,1,0,-1.5,0.6,5,30,0.0)
Camera(0,1.4,0.2,0,1,0,-0.5,0,-5,30,0)
-- add some lights
AmbientLight(0.25, 0.25, 0.25)
Light(-4, 4, 3, 1.0, 0.95, 0.92, 0.0, 0.0, 0.0, -1.0)
Light(-2, 4, 3.2, 0.5, 0.5, 0.5, 0.0, 0.0, 0.0, -1.0)
Light(3, 4, 3.2, 0.6, 0.2, 0.2, 0.0, 0.0, 0.0, -1.0)

identity = Transform() -- identity

--[[
Below here is just adding all of the individual objects in the scene. Tweak for 
desired location, material etc...
]]--
-- walls (cube)
wallmat = Material(0.2, 0.2, 0.01, 0.9, 0.8, 0.7, 0.4, 0.4, 0.4, 10.0, 0.0, 0.0, 0.0, 0.0, -1.0, -1.0, -1.0)
cube_tf = Transform(4.5,0,0,0,0,4.5,0,0,0,0,4.5,-1,0,0,0,1)
cube_id = Object("../res/cubeinv.obj")
--cube_tex = Texture("../res/stonetex.png")
Instance("object",cube_id,cube_tf,wallmat,0,false,0,false)

-- add floor
floor_tf = Transform(2,0,0,-0.5,0,0,-2,-0.65,0,2,0,-4,0,0,0,1)
floor_id = Object("../res/stonefloor.obj")
floormat = Material(0.1, 0.1, 0.1, 0.4, 0.4, 0.4, 1.0, 1.0, 1.0, 1.0, 0.3, 0.3, 0.3, 15, 0, 0, 0)
floortex = Texture("../res/stonetex.png")
Instance("object", floor_id, floor_tf, floormat, floortex, true, 0, false)
-- add shelf
shelf_tf = Transform(-0.25, 0, 0, 0, 0, 0, -0.25, 0, 0, 0.25, 0, -5, 0, 0, 0, 1)
shelf_mat = Material(0.2, 0.1, 0.0, 0.9, 0.7, 0.6, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0)
shelf_id = Object("../res/shelf.obj")
shelftex = Texture("../res/woodtex.png")
Instance("object", shelf_id, shelf_tf, shelf_mat, shelftex, true,0,false)

-- martini glass
martini_tf1 = Transform(0.09,0,0,-1.5,0,0.09,0,0.66,0,0,0.09,-5.2,0,0,0,1)
martini_tf2 = Transform(0.09,0,0,-1.0,0,0.09,0,0.66,0,0,0.09,-4.8,0,0,0,1)
martini_mat = Material(0.3,0.3,0.3,0.6,0.6,0.6,0.6,0.6,0.6,10.0,0.97,0.97,0.97,1.5,0.0,0.0,0.0)
martini_id = Object("../res/cup.obj")
Instance("object", martini_id, martini_tf1, martini_mat, 0, false, 0, false)
Instance("object", martini_id, martini_tf2, martini_mat, 0, false, 0, false)


-- add clay pot
vase_tf = Transform(0, 0, 0.12, -0.8, 0, 0.12, 0, 0.03, -0.12, 0, 0, -5.02, 0, 0, 0, 1)
vase_mat = Material(0.3, 0.1, 0.2, 0.4, 0.3, 0.1, 0.3, 0.3, 0.3, 10.0, 0, 0, 0, 0.0, 0.0, 0.0, 0.0)
vase_id = Object("../res/claypot.obj")
Instance("object", vase_id, vase_tf, vase_mat, 0, false,0,false)

-- and a refractive sphere for good measure
sphere_tf1 = Transform(1,0,0,-1.5,0,1,0,-0.207,0,0,1,-5,0,0,0,1)
sphere_tf2 = Transform(1,0,0,-1.3,0,1,0,-0.207,0,0,1,-4.8,0,0,0,1)
sphere_id = Sphere(0,0,0,0.12)
sphere_mat = Material(0.4,0.4,0.8,0.4,0.4,0.8,0.5,0.5,0.5,2.0,0.93,0.93,0.93,1.3,0.3,0.3,0)
Instance("sphere", sphere_id, sphere_tf1, sphere_mat, 0, false, 0, false)
Instance("sphere", sphere_id, sphere_tf2, sphere_mat, 0, false, 0, false)

-- add a bunny
bunny_tf = Transform(4,0,0,-0.15,0,4,0,-0.45,0,0,4,-4.93,0,0,0,1)
bunny_id = Object("../res/bunny.obj")
bunnymat = Material(0,0.1,0.1,0,0.4,0.6,0.5,0.6,0.6,10,0.8,0.8,0.8,20,0.3,0.3,0.1)
Instance("object",bunny_id,bunny_tf,bunnymat,0,false,0,false)

-- add plates
plate_id = Object("../res/chinaplate.obj")
plate_mat = Material(0.3,0.3,0.3,0.5,0.5,0.5,0.6,0.6,0.6,30,0.2,0.2,0.2,20,0,0,0)
plate_tf1 = Transform(0.2,0,0,0.5,0,0.2,0,-0.33,0,0,0.2,-4.82,0,0,0,1)
plate_tf2 = Transform(0.2,0,0,0.5,0,0.2,0,-0.29,0,0,0.2,-4.82,0,0,0,1)
plate_tf3 = Transform(0.2,0,0,0.5,0,0.2,0,-0.25,0,0,0.2,-4.82,0,0,0,1)
plate_tf4 = Transform(0.2,0,0,0.5,0,0.0347,-0.197,0,0,0.197,0.0347,-5.4,0,0,0,1)
Instance("object",plate_id,plate_tf1,plate_mat,0,false,0,false)
Instance("object",plate_id,plate_tf2,plate_mat,0,false,0,false)
Instance("object",plate_id,plate_tf3,plate_mat,0,false,0,false)
Instance("object",plate_id,plate_tf4,plate_mat,0,false,0,false)

-- a mug
mug_id = Object("../res/mug.obj")
mug_mat = Material(0.0,0.1,0.0,0.1,0.3,0.1,0.0,0.3,0.0,4,0.05,0.05,0.05,20,0,0,0)
mug_tf = Transform(0.13,0,0,0.7,0,0.13,0,0.66,0,0,0.13,-4.92,0,0,0,1)
Instance("object",mug_id,mug_tf,mug_mat,0,false,0,false)

-- and a picture!
pictex = Texture("../res/holidaytex.png")
pic_tf = Transform(0.7,0,0,-0.1,0,0.7,0,1.12,0,0,-0.7,-5.44,0,0,0,1)
pic_mat = Material(0.2,0.2,0.2,0.8,0.8,0.8,0,0,0,0,0,0,0,0,0,0,0)
Instance("object",floor_id,pic_tf,pic_mat,pictex,true,0,false)


--[[
Finally let it go!
]]--
-- set the BVH mode
BVHMode("tree")
-- render
Render()