-- clear everything
Clear()
-- set the resolution
Resolution(512, 512)
-- add some lights
AmbientLight(0.2, 0.2, 0.2)
Light(0.0, 10.0, 0.0, 1.0, 1.0, 1.0, 0.0, 0.0, 0.0, -1.0)
Light(3.0, 3.0, 5.0, 1.0, 1.0, 1.0, 0.0, 0.0, 0.0, 0.9)
-- add some materials
Material(1.0, 0.0, 0.0, 0.7, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0, -1.0, -1.0, -1.0) -- matte red
Material(0.0, 0.0, 1.0, 0.0, 0.0, 0.7, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0, -1.0, -1.0, -1.0) -- matte blue
Material(0.0, 1.0, 0.0, 0.0, 0.7, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0, -1.0, -1.0, -1.0) -- matte green

-- This is the sphere we are going to put a lot of
Sphere(0, 0, 0, 1);

Transform()

-- Make 100 transforms and instantiate 100 spheres
for i = 0, 39 do
  for j = 0, 39 do
    lx = -39.5 + 2 * j
    ly = 39.5 - 2 * i
    lz = -40
    tformid = Transform(1, 0, 0, lx, 0, 1, 0, ly, 0, 0, 1, lz, 0, 0, 0, 1)
    Instance("sphere", 0, tformid, i % 3, 0, false)
  end
end

-- Background
Vertex(-40, 40, -42, 0, 0, 0, 0, 0)
Vertex(-40, -40, -42, 0, 0, 0, 0, 0)
Vertex(40, -40, -42, 0, 0, 0, 0, 0)
Vertex(40, 40, -42, 0, 0, 0, 0, 0)

Triangle(0,1,2)
Triangle(2,3,0)
Instance("triangle",0,0,1,0,false)
Instance("triangle",1,0,1,0,false)

-- And go!
BVHMode("none",2)
Render()
