--[[
This scene is intended to replicate the CornellBox scene given in
Assignment 4 for benchmarking purposes.
]]--
Clear()
Camera(0,0,20,0,1,0,0,0,0,36.5,0)
AntiAlias(4)
Resolution(512,512)
AmbientLight(0.05, 0.05, 0.05)
Light(0, 9, -12.0, 1.0, 1.0, 1.0, 0.0, 0.0, 0.0, -1.0)
-- Materials
gray = Material(1, 1, 1, 0.8, 0.8, 0.8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 )
red = Material(1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
blue = Material(0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
mirrormat = Material(1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0.4, 0.4, 0.7, 0, 0.0, 0.0, 0.0)
-- Transform
identity = Transform(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, -12, 0, 0, 0, 1)
-- Walls
lbb = Vertex(-10,-10,-10)
lbf = Vertex(-10,-10,10)
rbf = Vertex(10,-10,10)
rbb = Vertex(10,-10,-10)
ltb = Vertex(-10,10,-10)
ltf = Vertex(-10,10,10)
rtf = Vertex(10,10,10)
rtb = Vertex(10,10,-10)
floortri1 = Triangle(lbb,lbf,rbf)
floortri2 = Triangle(lbb,rbf,rbb)
backtri1 = Triangle(lbb,rbb,ltb)
backtri2 = Triangle(ltb,rbb,rtb)
ceiltri1 = Triangle(ltf,ltb,rtb)
ceiltri2 = Triangle(ltf,rtb,rtf)
lefttri1 = Triangle(lbb,ltb,ltf)
lefttri2 = Triangle(ltf,lbf,lbb)
righttri1 = Triangle(rtf,rbb,rbf)
righttri2 = Triangle(rtf,rtb,rbb)
-- Spheres
diffsphere = Sphere(5,-6,1,4)
mirrsphere = Sphere(-5,-6,-5,4)
-- Instantiate everything
Instance("triangle",floortri1,identity,gray,0,false,0,false)
Instance("triangle",floortri2,identity,gray,0,false,0,false)
Instance("triangle",backtri1,identity,gray,0,false,0,false)
Instance("triangle",backtri2,identity,gray,0,false,0,false)
Instance("triangle",ceiltri1,identity,gray,0,false,0,false)
Instance("triangle",ceiltri2,identity,gray,0,false,0,false)
Instance("triangle",lefttri1,identity,red,0,false,0,false)
Instance("triangle",lefttri2,identity,red,0,false,0,false)
Instance("triangle",righttri1,identity,blue,0,false,0,false)
Instance("triangle",righttri2,identity,blue,0,false,0,false)
Instance("sphere",diffsphere,identity,gray,0,false,0,false)
Instance("sphere",mirrsphere,identity,mirrormat,0,false,0,false)
-- Add a bunny for good measure --
bunnytform1 = Transform(25,0,0,-5,0,25,0,-10.8,0,0,25,-9,0,0,0,1)
bunnytform2 = Transform(0,-40,0,11.2,40,0,0,5,0,0,40,-14,0,0,0,1)
bunnymat1 = Material(0,1,1,0,0.7,0.8,1,1,1,10,0.5,0.5,0.5,1.3,0,0,0)
bunnymat2 = Material(1,1,0,1,1,0,0,0,0,10,0,0,0,0,0,0,0)
Object("../res/bunny.obj")
Instance("object",0,bunnytform1,bunnymat1,0,false,0,false)
Instance("object",0,bunnytform2,bunnymat2,0,false,0,false)

-- Use tree BVH
BVHMode("tree")
-- Render
Render()

