-- Check if a file exists
function file_exists(file)
	local f = io.open(file, "r")
	if f then f:close() end
	return f ~= nil
end
-- This function reads in an object file and adds the vertices, triangles (and instances) to the scene
-- Arguments are AddObjToScene("<filename>", tform_id, mat_id, tex_id, bTextured)
-- This function will return the index of the first instance added, and also the number of instances added 
function AddObjToScene(filename, tform_id, mat_id, tex_id, bTextured)
  assert(file_exists(filename), "File not found...")
  -- Store the local stuff
  local v_s = {}
  local vt_s = {}
  local vn_s = {}
  local f_s = {}

for line in io.lines(filename) do
  local tokens = {}
  for t in string.gmatch(line, "%S+") do
    tokens[#tokens+1] = t
  end
  
  -- Get all the data
  if(tokens[1] == "v") then
    v_s[#v_s+1] = {tonumber(tokens[2]), tonumber(tokens[3]), tonumber(tokens[4])}
  elseif(tokens[1] == "vt") then
    vt_s[#vt_s+1] = {tonumber(tokens[2]) or 0.0, tonumber(tokens[3]) or 0.0}
  elseif(tokens[1] == "vn") then
    vn_s[#vn_s+1] = {tonumber(tokens[2]) or 0.0, tonumber(tokens[3]) or 0.0, tonumber(tokens[4]) or 0.0}
  elseif(tokens[1] == "f") then
    f_s[#f_s+1] = {tokens[2], tokens[3], tokens[4]}
  end
end
-- Add all vertices
for _,vertex in ipairs(v_s) do
  Vertex(vertex[1], vertex[2], vertex[3])
end
-- Add all triangles (and instances)
for _, tri in ipairs(f_s) do
tri_id = Triangle((tri[1]-1), (tri[2]-1), (tri[3]-1));
 --print((tri[1]-1).." "..(tri[2]-1).." "..(tri[3]-1))
Instance("triangle", tri_id, tform_id, mat_id, tex_id, bTextured)
end
end

--AddObjToScene("../res/shelf.obj", 0, 0, 0, false)


--for s in string.gmatch(str, "(%d+)/?") do




