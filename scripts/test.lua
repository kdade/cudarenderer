--dofile("../scripts/CornellBox.lua")

Clear()
Resolution(512,512)
AmbientLight(1,1,1)
Camera(0,3,0,0,1,0,0,0,-3,30,0)
Light(0,4,-3, 1.0, 1.0, 1.0, 0,0,-3, -1.0)
Transform(1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1)
Material(0.1,0.1,0.1,0.5,0.5,0.8,0,0,0,0,0,0,0,0,0,0,0)
Material(0.1,0.1,0.1,0.8,0.5,0.5,0,0,0,0,0,0,0,0,0,0,0)
Sphere(1,0,-2,1)
Sphere(-1,0,-2,1)
Instance("sphere",0,0,0,0,false,0,false)
Instance("sphere",1,0,1,0,false,0,false)

--[[Texture("../res/stanford.png")
Object("../res/cube.obj")
Instance("object",0,0,0,0,true,0,false)]]--
-- Use tree BVH
BVHMode("tree")
-- Render
Render()

--[[
-- Basic testing script --

-- clear everything
Clear()
AntiAlias(8)
-- set the resolution
Resolution(512, 512)
-- add some lights
AmbientLight(0.2, 0.2, 0.2)
Light(0.0, 10.0, 0.0, 1.0, 1.0, 1.0, 0.0, 0.0, 0.0, -1.0)
Light(3.0, 3.0, 5.0, 1.0, 1.0, 1.0, 0.0, 0.0, 0.0, 0.9)
-- add some materials
Material(1.0, 0.0, 0.0, 0.7, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0, -1.0, -1.0, -1.0) -- matte red
Material(0.0, 0.0, 1.0, 0.0, 0.0, 0.7, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0, -1.0, -1.0, -1.0) -- matte blue
Material(0.0, 1.0, 0.0, 0.0, 0.7, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0, -1.0, -1.0, -1.0) -- matte green

-- add transforms
Transform() -- identity
Transform(1, -0.5, 0.3, -1, 0.2, 1, 0.4, -1, 0, 0, 1, -1, 0, 0, 0, 1);
Transform(60, 0, 0, 0, 0, 60, 0, -4, 0, 0, 60, -20, 0, 0, 0, 1);
Transform(30, 0, 0, 5, 0, 30, 0, 2, 0, 0, 30, -10, 0, 0, 0, 1);
Transform(30, 0, 0, 3, 0, 30, 0, -10, 0, 0, 30, -10, 0, 0, 0, 1);
Transform(1, 0, 0, -3, 0, 1, 0, -2, 0, 0, 1, -4, 0, 0, 0, 1);
vase_tform = Transform(1, 0, 0, 2, 0, 0, -1, 2, 0, 1, 0, -5, 0, 0, 0, 1)



-- add a sphere
--Sphere(-2.0, -3.0, -10.0, 1.5)
--Sphere(2.0, 1.0, -4.0, 0.8)
--Sphere(-20.0, -20.0, -60.0, 30)

-- add a triangle
Vertex(-0.5, -0.5, -1)
Vertex(0.5, -0.5, -1)
Vertex(0.0, 0.5, -1)
Triangle(0,1,2)
Instance("triangle",0,3,1,0,false)

Object("../res/cubeflat.obj")
Object("../res/bunny.obj")
Object("../res/Kevin_GlassVase.obj")
Instance("object",0,5,0,0,false)
Instance("object",1,4,2,0,false)
Instance("object",2,vase_tform,0,0,false)

-- build the BVH
BVHMode("tree", 2)
-- render
Render()]]--