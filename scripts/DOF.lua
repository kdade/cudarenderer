--[[
This scene is intended to replicate the given DOF scene,
for benchmarking purposes..
]]--
Clear()
Camera(0,2,0,0,1,0,0,0,-5.5,55,0)
AntiAlias(8)
Resolution(512,512)
AmbientLight(0.1, 0.1, 0.1)
Light(0, 10, 0, 0.8, 0.8, 0.8, 0.0, 0.0, 0.0, -1.0)
-- Transforms
Transform()
-- Materials
gray = Material(1, 1, 1, 0.4, 0.4, 0.4, 0, 0, 0, 0.9, 0.9, 0.9, 0, 0, 0, 0, 0)
diffmat = Material(0.75, 0.75, 1, 0.75, 0.75, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
reflmat = Material(1, 0.75, 0.75, 1, 0.75, 0.75, 1, 1, 1, 50, 0.75, 0.75, 0.75, 0, 0, 0, 0)
thirdmat = Material(0, 1, 0, 0, 1, 0, 0.3, 0.3, 0.3, 50, 0.95, 0.98, 0.95, 1.8, 0, 0, 0)

-- Floor
Vertex(-1000,0,-10000)
Vertex(-1000,0,1000)
Vertex(1000,0,1000)
Vertex(1000,0,-10000)
Triangle(0,1,2)
Triangle(0,2,3)
Instance("triangle",0,0,gray,0,false)
Instance("triangle",1,0,gray,0,false)
-- Spheres
reflsphere = Sphere(1,1,-7,1)
diffsphere = Sphere(-1,1,-5,1)
focsphere = Sphere(0.2,0.5,-4.5,0.5)
Instance("sphere",reflsphere,0,reflmat,0,false)
Instance("sphere",diffsphere,0,diffmat,0,false)
Instance("sphere",focsphere,0,thirdmat,0,false)

--Do it to it
BVHMode("tree",2)
Render()
