-- clear everything
Clear()
-- set the resolution
Resolution(512, 512)
--AntiAlias(4)
-- set the view point
Camera(4,2.5,0,0,1,0,0,0,-5,30,0.0)
-- add some lights
AmbientLight(1.0, 1.0, 1.0)
--Light(0.0, 10.0, 0.0, 1.0, 1.0, 1.0, 0.0, 0.0, 0.0, -1.0)
Light(-6, 7, 3, 1.0, 1.0, 1.0, 0.0, 0.0, 0.0, -1.0)
-- add some materials
red = Material(0.2, 0.0, 0.0, 0.7, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, -1.0, -1.0, -1.0) -- matte red
blue = Material(0.0, 0.0, 0.2, 0.0, 0.0, 0.7, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, -1.0, -1.0, -1.0) -- matte blue
green = Material(0.0, 0.2, 0.0, 0.0, 0.7, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, -1.0, -1.0, -1.0) -- matte green


identity = Transform() -- identity

--[[
Build the back wall and floor
]]--
floormat = Material(0.1, 0.1, 0.1, 0.4, 0.4, 0.4, 0.0, 0.0, 0.0, 1.0, 0.2, 0.2, 0.2, 0.0, -1.0, -1.0, -1.0) -- matte green
wallmat = Material(0.2, 0.2, 0.2, 0.5, 0.5, 0.5, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, -1.0, -1.0, -1.0) -- matte green
room_tf = Transform(100,0,0,0,0,100,0,-0.68,0,0,100,-5.7,0,0,0,1)
l = Vertex(-1,0,0)
r = Vertex(1,0,0)
ru = Vertex(1,1,0)
lu = Vertex(-1,1,0)
lf = Vertex(-1,0,1)
rf = Vertex(1,0,1)
Triangle(lf,r,l)
Triangle(lf,rf,r)
Triangle(l,r,ru)
Triangle(l,ru,lu)
Instance("triangle", 0, room_tf, floormat, 0, false, 0,false)
Instance("triangle", 1, room_tf, floormat, 0, false ,0,false)
Instance("triangle", 2, room_tf, wallmat, 0, false,0,false)
Instance("triangle", 3, room_tf, wallmat, 0, false,0,false)

--[[
Below here is just adding all of the individual objects in the scene. Tweak for 
desired location, material etc...
]]--
-- add shelf
shelf_tf = Transform(-0.25, 0, 0, 0, 0, 0, -0.25, 0, 0, 0.25, 0, -5, 0, 0, 0, 1)
shelf_mat = Material(0.4, 0.2, 0.0, 0.8, 0.44, 0.2, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0)
shelf_id = Object("../res/shelf.obj")
Instance("object", shelf_id, shelf_tf, shelf_mat, 0, false,0,false)


-- add vase
vase_tf = Transform(0, 0, 0.12, 0, 0, 0.12, 0, 0.03, -0.12, 0, 0, -5.02, 0, 0, 0, 1)
vase_mat = Material(0.4, 0.2, 0.2, 0.7, 0.5, 0.4, 0.5, 0.5, 0.5, 10.0, 0, 0, 0, 0.0, 0.0, 0.0, 0.0)
vase_id = Object("../res/claypot.obj")
--Texture("../res/stanford.png")
Instance("object", vase_id, vase_tf, vase_mat, 0, true,0,false)


--[[
Finally let it go!
]]--
-- set the BVH mode
BVHMode("tree")
-- render
Render()